from django.contrib import admin
from django.utils.translation import gettext as _
from django.contrib.auth.admin import UserAdmin
from django.conf import settings
from django.utils.safestring import mark_safe
import os

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from induction.models.electrogasuser import AdminUser
from induction.models.video import Video
from induction.models.course import Course
from induction.models.question import Question
from induction.models.option import Option
from induction.models.contractor import Contractor
from induction.models.employee import Employee
from induction.models.visitor import Visitor
from induction.models.user_course import UserCourse
from induction.models.user_course_question_answer import UserCourseQuestionAnswer
from induction.models.site_settings import SiteSetting
from induction.models.area import Area

from induction.forms.courseform import CourseForm
from induction.forms.videoform import VideoForm
from induction.forms.electrogasuserform import ElectrogasUserChangeForm, ElectrogasUserCreationForm
from induction.forms.contractorform import ContractorChangeForm, ContractorCreationForm
from induction.forms.employeeform import EmployeeChangeForm, EmployeeCreationForm
from induction.forms.visitorform import VisitorChangeForm, VisitorCreationForm
from induction.forms.validationform import QuestionFormInlineOptionValidation
from induction.forms.sitesettingsform import SiteSettingsForm
from induction.forms.historyform import HistoryForm
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
# from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter
from admin_reports import Report, register
from django.db.models.functions import Lower
import urllib.parse
import datetime, pytz
from django.db import connection

@register()
class HistoryReport(Report):
    form_class = HistoryForm
    title = 'User Course History'
    list_per_page = 20
    help_text = '<ul style="margin-left: 0px;margin-bottom: 0;"><li>Click on course to see question-answer history.</li><li>Click on username to see profile picture.</li></ul>'

    def get_contractor_img_url(self, obj):
    	return obj.contractor_photo.url if obj.contractor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)

    def get_employee_img_url(self, obj):
    	return obj.employee_photo.url if obj.employee_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)

    def get_visitor_img_url(self, obj):
    	return obj.visitor_photo.url if obj.visitor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)

    def get_adminuser_img_url(self, obj):
    	return obj.user_photo.url if obj.user_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)

    def change_column_name(self,obj):
    	img_url = ''
    	if User.objects.get(username=obj['user__username']).groups.all().filter(name='Contractor').exists():
    		img_url = self.get_contractor_img_url(Contractor.objects.get(username=obj['user__username']))
    	elif User.objects.get(username=obj['user__username']).groups.all().filter(name='Employee').exists():
    		img_url = self.get_employee_img_url(Employee.objects.get(username=obj['user__username']))
    	elif User.objects.get(username=obj['user__username']).groups.all().filter(name='Visitor').exists():
    		img_url = self.get_visitor_img_url(Visitor.objects.get(username=obj['user__username']))
    	else:
    		if AdminUser.objects.filter(username=obj['user__username']).exists():
    			img_url = self.get_adminuser_img_url(AdminUser.objects.get(username=obj['user__username']))
    		else:
    			img_url = os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)

    	obj['img_url']= img_url
    	obj['course']= obj.pop('course__course_title')
    	obj['username']= obj.pop('user__username')
    	obj['status']= obj.pop('user_course_status')
    	obj['result']= obj.pop('user_course_result')
    	obj['percentage']= obj.pop('user_course_obtained_percentage')
    	obj['course_complete_date']= obj.pop('user_course_complete_date')
    	obj['expiry_date']= obj.pop('user_course_expiry_date')

    	user_course_validity = obj.pop('user_course_validity')

    	if user_course_validity == 0:
    		obj['expiry_date'] = None
    		
    	employee_list=[]
    	if self._request.user.groups.all().filter(name='Contractor').exists():
    		employee_list = Employee.objects.filter(employee_contractor=self._request.user.id).values_list('id')
    		employee_list= [item for t in list(employee_list) for item in t]
    		if employee_list and User.objects.get(username=obj['username']).id in employee_list:
    			return obj
    		else:
    			{}
    	else:
	    	return obj

    def aggregate(self, **kwargs):
    	history = UserCourse.objects.annotate(course_lower=Lower('course__course_title')).values('id', 'course__course_title', 'user__username', 'user_course_status', 'user_course_result', 'user_course_obtained_percentage', 'user_course_complete_date', 'user_course_expiry_date', 'user_course_validity')

    	# add not attempted user START
    	history_cursor = connection.cursor()
    	history_cursor.execute('''SELECT * FROM ((SELECT user_ptr_id as user_id, induction_adminuser_user_course.course_id, induction_usercourse.id FROM induction_adminuser LEFT JOIN induction_adminuser_user_course ON induction_adminuser_user_course.adminuser_id = induction_adminuser.user_ptr_id LEFT JOIN induction_usercourse ON induction_usercourse.user_id = induction_adminuser.user_ptr_id AND induction_usercourse.course_id = induction_adminuser_user_course.course_id) UNION ALL (SELECT user_ptr_id as user_id, induction_contractor_contractor_course.course_id,  induction_usercourse.id FROM induction_contractor LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id LEFT JOIN induction_usercourse ON induction_usercourse.user_id = induction_contractor.user_ptr_id AND induction_usercourse.course_id = induction_contractor_contractor_course.course_id) UNION ALL (SELECT user_ptr_id as user_id, induction_visitor_visitor_course.course_id, induction_usercourse.id FROM induction_visitor LEFT JOIN induction_visitor_visitor_course ON induction_visitor_visitor_course.visitor_id = induction_visitor.user_ptr_id LEFT JOIN induction_usercourse ON induction_usercourse.user_id = induction_visitor.user_ptr_id AND induction_usercourse.course_id = induction_visitor_visitor_course.course_id) UNION ALL (SELECT induction_employee.user_ptr_id as user_id, induction_contractor_contractor_course.course_id, induction_usercourse.user_id FROM induction_employee LEFT JOIN induction_contractor ON induction_contractor.user_ptr_id = induction_employee.employee_contractor_id LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id LEFT JOIN induction_usercourse ON induction_usercourse.user_id = induction_employee.user_ptr_id AND induction_usercourse.course_id = induction_contractor_contractor_course.course_id)) AS GroupTable WHERE course_id is not null AND id is NULL''')

    	not_attempted_user_list = []
    	for h in history_cursor.fetchall():
    		is_matched = True

    		if kwargs['course'] and len(kwargs['course']):
    			if kwargs['course'].lower() not in Course.objects.get(pk=h[1]).course_title.lower():
    				is_matched = False

    		if kwargs['result'] and len(kwargs['result']):
    			if kwargs['result'] not in ['All', 'Pending']:
    				is_matched=False

    		if kwargs['status'] and len(kwargs['status']):
    			if kwargs['status'] not in ['All', 'Not Attempted']:
    				is_matched=False

    		if kwargs['course_complete_date_from'] or kwargs['course_complete_date_to']:
    			is_matched=False

    		if is_matched:
	    		not_attempted_user_list.append({'id': h[2], 'course__course_title': Course.objects.get(pk=h[1]).course_title, 'user__username': User.objects.get(pk=h[0]).username, 'user_course_status': 'Not Attempted', 'user_course_result': 'Pending', 'user_course_obtained_percentage': None, 'user_course_complete_date': None, 'user_course_expiry_date': None, 'user_course_validity': UserCourse.objects.get(pk=h[2]).user_course_validity if h[2] else None })
    	# add not attempted user END

    	if kwargs['course'] and len(kwargs['course']):
    		history = history.filter(course_lower__contains=str(kwargs['course']).lower())

    	if kwargs['result'] and len(kwargs['result']) and kwargs['result'] != 'All':
    		history = history.filter(user_course_result=kwargs['result'])

    	if kwargs['status'] and len(kwargs['status']) and kwargs['status'] != 'All':
    		history = history.filter(user_course_status=kwargs['status'])

    	if kwargs['course_complete_date_from'] and kwargs['course_complete_date_to']:
    		from_date_str = urllib.parse.unquote(kwargs['course_complete_date_from'])
    		to_date_str = urllib.parse.unquote(kwargs['course_complete_date_to'])

    		from_date_naive = datetime.datetime.strptime(from_date_str, '%m/%d/%Y %H:%M:%S')
    		to_date_naive = datetime.datetime.strptime(to_date_str, '%m/%d/%Y %H:%M:%S')

    		from_date = pytz.timezone(settings.TIME_ZONE).localize(from_date_naive)
    		to_date =  pytz.timezone(settings.TIME_ZONE).localize(to_date_naive)

    		history = history.filter(user_course_complete_date__range=[from_date,to_date])
    	elif kwargs['course_complete_date_from']:
    		from_date_str = urllib.parse.unquote(kwargs['course_complete_date_from'])
    		from_date_naive = datetime.datetime.strptime(from_date_str, '%m/%d/%Y %H:%M:%S')
    		from_date = pytz.timezone(settings.TIME_ZONE).localize(from_date_naive)
    		history = history.filter(user_course_complete_date__gte=from_date)	
    	elif kwargs['course_complete_date_to']:
    		to_date_str = urllib.parse.unquote(kwargs['course_complete_date_to'])
    		to_date_naive = datetime.datetime.strptime(to_date_str, '%m/%d/%Y %H:%M:%S')
    		to_date =  pytz.timezone(settings.TIME_ZONE).localize(to_date_naive)
    		history = history.filter(user_course_complete_date__lte=to_date)

    	result_dict = list(history) + not_attempted_user_list
    	result_dict = list(map(lambda r: self.change_column_name(r),result_dict))
    	result_dict = list(filter(None,result_dict))

    	return result_dict

class OptiontInline(admin.TabularInline):
    model = Option
    extra = 1
    formset = QuestionFormInlineOptionValidation

class QuestionAdmin(admin.ModelAdmin):
	def mark_active(modeladmin, request, queryset):
	    queryset.update(question_status='Active')
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(question_status='Inactive')
	mark_inactive.short_description = "Mark selected as Inactive"

	list_display = ('question_title','question_weightage', 'question_status')
	search_fields = ('question_title', 'question_weightage', 'question_status')
	list_filter = ('question_status',)
	# readonly_fields= ['question_created_date', 'question_updated_date',]
	ordering = ('id',)
	inlines = [OptiontInline]
	list_per_page = 10
	actions = [mark_active, mark_inactive]

class VideoAdmin(admin.ModelAdmin):
	def mark_active(modeladmin, request, queryset):
	    queryset.update(video_status='Active')
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(video_status='Inactive')
	mark_inactive.short_description = "Mark selected as Inactive"

	form = VideoForm
	list_display = ('video_title','video_url', 'video_status')
	search_fields = ('video_title', 'video_url', 'video_status')
	list_filter = ('video_status',)
	# readonly_fields= ('video_type',)
	ordering = ('id',)
	list_per_page = 10
	actions = [mark_active, mark_inactive]

	fieldsets = fieldsets = ((None, {'fields': ('video_title', 'video_url', 'video_question', 'video_status')}),)

class CourseAdmin(admin.ModelAdmin):
	def mark_active(modeladmin, request, queryset):
	    queryset.update(course_status='Active')
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(course_status='Inactive')
	mark_inactive.short_description = "Mark selected as Inactive"

	form = CourseForm
	list_display = ('course_title','course_validity', 'course_passing_percentage', 'course_status')
	search_fields = ('course_title', 'course_status', 'course_validity', 'course_passing_percentage')
	list_filter = ('course_status',)
	# readonly_fields= ['course_created_date', 'course_updated_date',]
	ordering = ('id',)
	list_per_page = 10
	actions = [mark_active, mark_inactive]

class ContractorUserAdmin(UserAdmin):
	def user_image(self, obj):
		img_url = obj.contractor_photo.url if obj.contractor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="125" height="140" />'.format(url = img_url))
	
	def user_picture(self, obj):
		img_url = obj.contractor_photo.url if obj.contractor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="45" height="45" />'.format(url = img_url))
	
	def mark_active(modeladmin, request, queryset):
	    queryset.update(is_active=True)
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(is_active=False)
	mark_inactive.short_description = "Mark selected as Inactive"

	# Override add_fieldsets values of UserAdmin as per need of contractor admin
	add_fieldsets = ((None, {'classes': ('wide',),'fields': ('username', 'password1', 'password2', 'email', 'contractor_company_name', 'contractor_id_card_number'),}),)

	# Override list_display values of UserAdmin as per need of contractor admin
	list_display = ('user_picture', 'username', 'contractor_company_name', 'contractor_id_card_number', 'contractor_phone_number', 'is_active')

	# Override fieldset values of UserAdmin as per need of contractor admin
	fieldsets = fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('user_image', 'contractor_photo', 'first_name', 'last_name', 'email', 'contractor_phone_number')}),
        (_('Others'), {'fields': ('contractor_id_card_number',  'contractor_company_name' )}),
        (_('Course'),{'fields':('contractor_course',)}),
        (_('Status'), {
            'fields': ('is_active',),
        }),
    )

    # append readonly_fields values of contractor in UserAdmin as per need
	readonly_fields = UserAdmin.readonly_fields + ('user_image',)

	# Override list_filter values of UserAdmin as per need of contractor admin
	list_filter = ('is_active',)

	# Override actions of UserAdmin as per need of contractor admin
	actions = UserAdmin.readonly_fields + (mark_active, mark_inactive)

	form = ContractorChangeForm
	add_form = ContractorCreationForm
	search_fields = UserAdmin.search_fields +('contractor_company_name', 'contractor_id_card_number', 'contractor_phone_number','contractor_course__course_title')
	ordering = ('id',)
	list_per_page = 5

class EmployeeUserAdmin(UserAdmin):
	def get_queryset(self, request):
		from django.http import HttpResponse
		qs = super(EmployeeUserAdmin, self).get_queryset(request)
		if request.user.groups.all().filter(name='Contractor').exists():
			return qs.filter(employee_contractor=request.user)
		return qs

	def user_image(self, obj):
		img_url = obj.employee_photo.url if obj.employee_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="125" height="140" />'.format(url = img_url))
	
	def user_picture(self, obj):
		img_url = obj.employee_photo.url if obj.employee_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="45" height="45" />'.format(url = img_url))
	
	def mark_active(modeladmin, request, queryset):
	    queryset.update(is_active=True)
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(is_active=False)

	mark_inactive.short_description = "Mark selected as Inactive"

	raw_id_fields=('employee_contractor',)

	# Override add_fieldsets values of UserAdmin as per need of employee admin
	add_fieldsets = ((None, {'classes': ('wide',),'fields': ('username', 'password1', 'password2', 'email', 'employee_area', 'employee_contractor'),}),)

	# Override list_display values of UserAdmin as per need of employee admin
	list_display = ('user_picture', 'employee_id_card_number', 'employee_area', 'employee_phone_number', 'employee_contractor', 'is_active')

	# Override fieldset values of UserAdmin as per need of employee admin
	fieldsets = fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('user_image', 'employee_photo', 'first_name', 'last_name', 'email', 'employee_phone_number')}),
        (_('Others'), {'fields': ('employee_id_card_number', 'employee_area', 'employee_contractor' )}),
        # (_('Course'),{'fields':('employee_course',)}),
        (_('Status'), {
            'fields': ('is_active',),
        }),
    )

    # append readonly_fields values of employee in UserAdmin as per need
	readonly_fields = UserAdmin.readonly_fields + ('user_image',)

	# Override list_filter values of UserAdmin as per need of employee admin
	list_filter = ('is_active','employee_area',)

	# Override actions of UserAdmin as per need of employee admin
	actions = UserAdmin.readonly_fields + (mark_active, mark_inactive)

	form = EmployeeChangeForm
	add_form = EmployeeCreationForm

	search_fields = UserAdmin.search_fields +('employee_id_card_number', 'employee_phone_number', 'employee_contractor__contractor_company_name', 'employee_contractor__contractor_id_card_number', 'employee_contractor__username', 'employee_area__area_name')
	ordering = ('id',)
	list_per_page = 5

class VisitorUserAdmin(UserAdmin):
	def user_image(self, obj):
		img_url = obj.visitor_photo.url if obj.visitor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="125" height="140" />'.format(url = img_url))
	
	def user_picture(self, obj):
		img_url = obj.visitor_photo.url if obj.visitor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="45" height="45" />'.format(url = img_url))
	
	def mark_active(modeladmin, request, queryset):
	    queryset.update(is_active=True)
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(is_active=False)
	mark_inactive.short_description = "Mark selected as Inactive"

	def full_name(self,obj):
		return obj.get_full_name()

	# Override add_fieldsets values of UserAdmin as per need of visitor admin
	add_fieldsets = ((None, {'classes': ('wide',),'fields': ('username', 'password1', 'password2', 'email'),}),)

	# Override list_display values of UserAdmin as per need of visitor admin
	list_display = ('user_picture', 'full_name', 'visitor_id_card_number', 'visitor_phone_number', 'visitor_datetime_of_visit', 'visitor_employee', 'is_active')

	# Override fieldset values of UserAdmin as per need of visitor admin
	fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('user_image', 'visitor_photo', 'first_name', 'last_name', 'email', 'visitor_phone_number')}),
        (_('Others'), {'fields': ('visitor_id_card_number', 'visitor_employee', 'visitor_datetime_of_visit', 'visitor_purpose_of_visit')}),
        (_('Course'),{'fields':('visitor_course',)}),
        (_('Status'), {
            'fields': ('is_active',),
        }),
    )

    # append readonly_fields values of visitor in UserAdmin as per need
	readonly_fields = UserAdmin.readonly_fields + ('user_image',)

	# Override list_filter values of UserAdmin as per need of visitor admin
	list_filter = ('is_active', ('visitor_datetime_of_visit', DateRangeFilter))

	# Override actions of UserAdmin as per need of visitor admin
	actions = UserAdmin.readonly_fields + (mark_active, mark_inactive)

	form = VisitorChangeForm
	add_form = VisitorCreationForm
	# Override search_fields of UserAdmin as per need of visitor admin
	search_fields = UserAdmin.search_fields +('visitor_id_card_number', 'visitor_phone_number', 'visitor_employee', 'visitor_purpose_of_visit','visitor_course__course_title')
	ordering = ('id',)
	list_per_page = 5

class UserCourseAdmin(admin.ModelAdmin):
	readonly_fields = ('user_course_attempt_date',)

class SiteSettingsAdmin(admin.ModelAdmin):
	form = SiteSettingsForm
	list_display = ('site_name',)
	readonly_fields = ('site_name',)

class ElectrogasUserAdmin(UserAdmin):
	form = ElectrogasUserChangeForm
	add_form = ElectrogasUserCreationForm
	readonly_fields = UserAdmin.readonly_fields + ('user_image',)

	def user_image(self, obj):
		img_url = obj.user_photo.url if obj.user_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="125" height="140" />'.format(url = img_url))
	
	def user_picture(self, obj):
		img_url = obj.user_photo.url if obj.user_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
		return mark_safe('<img src="{url}" width="45" height="45" />'.format(url = img_url))
	
	def mark_active(modeladmin, request, queryset):
	    queryset.update(is_active=True)
	mark_active.short_description = "Mark selected as Active"

	def mark_inactive(modeladmin, request, queryset):
	    queryset.update(is_active=False)
	mark_inactive.short_description = "Mark selected as Inactive"

	actions = UserAdmin.readonly_fields + (mark_active, mark_inactive)
	list_display = ('user_picture',) + UserAdmin.list_display + ('is_active',)
	search_fields = UserAdmin.search_fields +('user_id_card_number','user_course__course_title','user_phone_number')
	ordering = ('id',)
	list_per_page = 5
	add_fieldsets = ((None, {'classes': ('wide',),'fields': ('username', 'password1', 'password2', 'email'),}),)

	fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('user_image', 'user_photo','first_name', 'last_name', 'email', 'user_phone_number', 'user_id_card_number')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'user_permissions'),
        }),
        (_('Courses'), {'fields': ('user_course',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
		

# Register your models here.
admin.site.unregister(User)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Course, CourseAdmin)

admin.site.register(AdminUser, ElectrogasUserAdmin)
admin.site.register(Contractor, ContractorUserAdmin)
admin.site.register(Employee, EmployeeUserAdmin)
admin.site.register(Visitor, VisitorUserAdmin)

# admin.site.register(UserCourse, UserCourseAdmin)
# admin.site.register(UserCourseQuestionAnswer)
admin.site.register(SiteSetting, SiteSettingsAdmin)
admin.site.register(Area)