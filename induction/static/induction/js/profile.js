$( document ).ready(function() {

$("#id_card").blur(function(){

    if($("#company_name").val()){
      if($("#company_name").val().length && $("#id_card").val().length)
      {
        $.ajax({
            type: "GET",
            url: window.location.origin + "/contractor/" + $("#company_name").val() + "/" + $("#id_card").val(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
              if(data){
                if($('#contractorexist_error').length){
                  $('#contractorexist_error').remove();
                }
                $( "<span id='contractorexist_error' style='margin-top:5px;margin-bottom:0px;color:#cc4141'>Id card number already registered with the given company name, kindly use another one</span>" ).insertAfter($("#id_card"));

                $("#id_card").focus();

                setTimeout(function(){ 
                  $("#id_card").val("");
                  $('#contractorexist_error').fadeOut('10000');
                }, 3500);
                
              }
              else{
                if($('#contractorexist_error').length){
                  $('#contractorexist_error').remove();
                }
              }
            },
            failure: function () {
                console.log("Failed to check contractor!")
            }
        });
      }
    }

    if($("#id_card").val())
    {
      $.ajax({
            type: "GET",
            url: window.location.origin + "/checkid/" + $("#id_card").val(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
              if(data){
                if($('#id_card_exist_error').length){
                  $('#id_card_exist_error').remove();
                }
                $( "<span id='id_card_exist_error' style='margin-top:5px;margin-bottom:0px;color:#cc4141'>Id card number already exist</span>" ).insertAfter($("#id_card"));

                $("#id_card").focus();

                setTimeout(function(){ 
                  $("#id_card").val("");
                  $('#id_card_exist_error').fadeOut('10000');
                }, 3500);
                
              }
              else{
                if($('#id_card_exist_error').length){
                  $('#id_card_exist_error').remove();
                }
              }
            },
            failure: function () {
                console.log("Failed to check contractor!")
            }
        });
    }

  });

  // ----------------------------------------------------validations-----------------------------------------------------

  if($('#user_group').val() != 'Employee')
  {
    $("#id_card").attr('placeholder', 'Id Card Number')
  }
  else
  {
    $("#id_card").attr('placeholder', 'ID/Passport Number') 
  }

	$.validator.addMethod("phone_number", function(value, element) {
	  return this.optional(element) || /^(?!-.*$)([+]?)(?!-.*$)([0-9-]){8,14}?$/.test(value);
	}, "Provide a valid phone number");


  $("#profile").validate({
      rules: {
        first_name: {
          required: true,
          maxlength: 30
        },
        last_name: {
          required: true,
          maxlength: 30
        },
        phone:{
          phone_number: true
        },
        id_card:{
          required: {
            depends: function(element) {
                return $('#user_group').val() == "Contractor" ? false : true;
            }
          },
          maxlength: 20
        },
      },
      messages: {
        first_name: {
          required: "Please enter your first name",
          maxlength: "Maximum length of first name is 30 characters"
        },
        last_name: {
          required: "Please enter your last name",
          maxlength: "Maximum length of last name is 30 characters"
        },
        id_card:{
          required: "ID is required",
          maxlength: "Maximum length of id card number is 20"
        }
      }
    })
});