$( document ).ready(function() {

	// call to get contractor list
	$.ajax({
        type: "GET",
        url: window.location.origin + "/contractors/",
        // data: '{name: "abc" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            $('#contractor_name').find('option').remove();
            $("#contractor_name").append($("<option/>").val("").text("Select one..."));
        	// console.log(data);
            $.each(data, function (){
              if(this.contractor_is_main)
              {
                var select_val=this.username;
                var select_text = '';
                if (this.contractor_id_card_number)
                {
                  select_text = this.contractor_company_name.toString() + '-' + this.contractor_id_card_number.toString();
                }
                else
                {
                  select_text = this.contractor_company_name.toString();
                }

                  $("#contractor_name").append($("<option/>").val(select_val).text(select_text));
              }
            });
        },
        failure: function () {
        	console.log("Failed to get contractors!")
        }
    });

  // call to get area list
  $.ajax({
        type: "GET",
        url: window.location.origin + "/areas/",
        // data: '{name: "abc" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            $('#area_name').find('option').remove();
            $("#area_name").append($("<option/>").val('').text("Select area"));
          // console.log(data);
            $.each(data, function (){
              var select_val=this.id;
              var select_text = this.area_name.toString();

                $("#area_name").append($("<option/>").val(select_val).text(select_text));
            });
        },
        failure: function () {
          console.log("Failed to get areas!")
        }
    });


    $('#employee_name').find('option').remove();
    $("#employee_name").append($("<option/>").val('').text("Select area to see employee"));

    // call to get employee list by area
  // $("#area_name").change(function(e){
  //   if($("#area_name").val())
  //   {
  //       $.ajax({
  //         type: "GET",
  //         url: window.location.origin + "/employee/" + $("#area_name").val(),
  //         // data: '{name: "abc" }',
  //         contentType: "application/json; charset=utf-8",
  //         dataType: "json",
  //         success: function(data){
  //             $('#employee_name').find('option').remove();
  //             $("#employee_name").append($("<option/>").val('').text("Select Employee"));
  //           // console.log(data);
  //             $.each(data, function (){
  //               var select_val=this.username.toString();
  //               var select_text = this.first_name.toString() + ' ' + this.last_name.toString();

  //                 $("#employee_name").append($("<option/>").val(select_val).text(select_text));
  //             });
  //         },
  //         failure: function () {
  //           console.log("Failed to get employee!")
  //         }
  //     });
  //   }
  //   else
  //   {
  //     $('#employee_name').find('option').remove();
  //     $("#employee_name").append($("<option/>").val('').text("Select area to see employee"));
  //   }
  // });

	// check if email exist on change of email field
    $("#email").blur(function(){
    	if($("#email").val().length)
    	{
    		$.ajax({
		        type: "GET",
		        url: window.location.origin + "/user/" + $("#email").val(),
		        // data: '{name: "abc" }',
		        contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function(data){
		        	if(data){
		        		if($('#userexist_error').length){
		        			$('#userexist_error').remove();
		        		}
		        		$( "<span id='userexist_error' style='margin-top:5px;margin-bottom:0px;color:#cc4141'>User already exist, kindly use another one</span>" ).insertAfter($("#email"));

		        		$("#email").focus();

		        		setTimeout(function(){ 
		        			$("#email").val("");
		        			$('#userexist_error').fadeOut('10000');
		        		}, 2500);
		        		
		        	}
		        	else{
		        		if($('#userexist_error').length){
		        			$('#userexist_error').remove();
		        		}
		        	}
		        },
		        failure: function () {
		            console.log("Failed to check user!")
		        }
		    });
    	}
	});


  //   $("#company_name").blur(function(){
  //     if($("#company_name").val().length && $("#id_card").val().length)
  //     {
  //       $.ajax({
  //           type: "GET",
  //           url: window.location.origin + "/contractor/" + $("#company_name").val() + "/" + $("#id_card").val(),
  //           contentType: "application/json; charset=utf-8",
  //           dataType: "json",
  //           success: function(data){
  //             if(data){
  //               if($('#contractorexist_error').length){
  //                 $('#contractorexist_error').remove();
  //               }
  //               $( "<span id='contractorexist_error' style='margin-top:5px;margin-bottom:0px;color:#cc4141'>Id card number already registered with the given company name, kindly use another one</span>" ).insertAfter($("#id_card"));

  //               // $("#id_card").focus();

  //               setTimeout(function(){ 
  //                 // $("#id_card").val("");
  //                 $('#contractorexist_error').fadeOut('10000');
  //               }, 3500);
                
  //             }
  //             else{
  //               if($('#contractorexist_error').length){
  //                 $('#contractorexist_error').remove();
  //               }
  //             }
  //           },
  //           failure: function () {
  //               console.log("Failed to check contractor!")
  //           }
  //       });
  //     }
  // });

  // $("#id_card").blur(function(){
  //     $("#company_name").blur();
  // });      

	// ============================== Autocomplete Script START =================================
	$.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";

        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value

        $( "<span id='autocomplete_error' style='margin-top:5px;margin-bottom:0px;color:#cc4141'>Did not match any search</span>" ).insertAfter(this.input);
        this.input
          .val( "" )
          // .attr({"title": value + " didn't match any item"} )
          // .tooltip( "open" );
        this.element.val( "" );
        this.input.focus();
        this._delay(function() {
          // this.input.tooltip( "close" ).attr( "title", "" );
          $( "#autocomplete_error" ).fadeOut('10000');
        }, 2500 );

        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#contractor_name" ).combobox();
    
    // $( "#employee_name" ).combobox();
    // ============================== Autocomplete Script END =================================


    $('#visit_datetime').datetimepicker({format:'m/d/Y H:i:s',})

    // $("#company_name").hide();
    // $("#contractor_name").hide();
    $("#company_contractor_div").hide();
    
    // $(".custom-combobox").hide();
    $(".ui-helper-hidden-accessible").hide();
	
	if ($('input[type=radio][name=user_type]').val()=='Visitor')
	{
		$("#visitor_div").show();

		// $("#company_name").hide();
		// $("#contractor_name").hide();
		// $("#company_contractor_div").hide();
	    $("#company_contractor_div").show();
	    $("#area_name").show();
	    // $("#company_name").hide();
	    $("#contractor_name").hide();

		$(".custom-combobox").hide();
	    // $(".custom-combobox").show();
	    // $(".custom-combobox-input").attr('placeholder','Employee Name');
		$("#company_contractor_div :input").val("");

	    $("#id_card").addClass("mandatory");
	    $("#id_card").attr("placeholder", "Id Card Number")
	}
	else if($('input[type=radio][name=user_type]').val()=='Contractor')
	{
		$("#visitor_div").hide();
		$("#visitor_div :input").val("");

		// $(".custom-combobox").hide();
		$(".custom-combobox").show();
		$(".custom-combobox-input").attr('placeholder','Contractor Name');
		$(".custom-combobox :input").val("");
		$("#contractor_name").val("");

		$("#company_contractor_div").show();
		// $("#company_name").show();
    	$("#area_name").hide();
    	$("#contractor_name").hide();
		// $("#contractor_name").hide();

    	$("#id_card").removeClass("mandatory");
    	$("#id_card-error").remove();
		$("#id_card").attr("placeholder", "Id Card Number")
	}
	else if($('input[type=radio][name=user_type]').val()=='Employee')
	{
		$(".custom-combobox-input").attr('placeholder','Contractor Name');
		$(".custom-combobox").show();

		$("#visitor_div").hide();
		$("#visitor_div :input").val("");

		$("#company_contractor_div").show();
    	$("#contractor_name").show();
		// $("#company_name").hide();
    	$("#area_name").show();
		// $("#company_name").val("");
		// $("#contractor_name").show("");

	    $("#id_card").addClass("mandatory");
	    $("#id_card").attr("placeholder", "ID/Passport Number")
	}

  function validate_file_size() {
    $("#file_size_error").html("");
    if ($('#user_image')[0].files[0])
    {
      var file_size = $('#user_image')[0].files[0].size;
      if(file_size>209716) {
        $("#file_size_error").html("Max file size is 200 kb");
        return false;
      } 
    }
    // else
    // {
    //   console.log("Hello");
    //   return false;
    // }
    return true;
  }

  $("#user_image").change(function(e){
    	$("#file_text").val(e. target. files[0]. name);
      if($("#file_text").val() && $("#file_text-error"))
      {
        $("#file_text-error").hide();
      }
      validate_file_size();
	});

  $("#register").submit(function(e){
      var file_validate= validate_file_size();
      // console.log(file_validate);
      if(!file_validate){
        e.preventDefault();
      }
  });

	$('input[type=radio][name=user_type]').change(function() {
	    if (this.value == 'Visitor') {

	    	$("#visitor_div").show();

	    	// $("#company_name").hide();
	    	// $("#company_name-error").remove();
    		// $("#contractor_name").hide();
    		$("#company_contractor_div").show();
        	$("#area_name").show();
        	// $("#company_name").hide();
        	$("#contractor_name").hide();
	    	
	    	$(".custom-combobox").hide();
        	// $(".custom-combobox").show();
        	// $(".custom-combobox-input").attr('placeholder','Employee Name');
	    	$("#company_contractor_div :input").val("");

        	$("#id_card-error").remove();
        	$("#id_card").addClass("mandatory");
        	$("#id_card").attr("placeholder", "Id Card Number")
	    }
	    else if (this.value == 'Contractor') {
	    	$("#visitor_div").hide();
	    	$("#visitor_div :input").val("");

	    	// $(".custom-combobox").hide();
	    	$(".custom-combobox").show();
	    	$(".custom-combobox-input").attr('placeholder','Contractor Name');
			$(".custom-combobox :input").val("");
	    	$("#contractor_name").val("");

	    	$("#company_contractor_div").show();
	    	// $("#company_name").show();
        	$("#area_name").hide();
	        $("#area_name-error").remove();
	        $("#contractor_name").hide();
		    // $("#contractor_name").hide();
	        $("#id_card").removeClass("mandatory");
	        $("#id_card-error").remove();
	        $("#id_card").attr("placeholder", "Id Card Number")
	    }
	     else if (this.value == 'Employee') {
	     	$(".custom-combobox-input").attr('placeholder','Contractor Name');
	     	$(".custom-combobox").show();

	     	$("#visitor_div").hide();
	    	$("#visitor_div :input").val("");

	    	$("#company_contractor_div").show();
	    	// $("#company_name").hide();
        	$("#area_name").show();
	    	// $("#company_name-error").remove();
	    	// $("#company_name").val("");

	    	// $("#contractor_name").show();
	        $("#id_card-error").remove();
	        $("#id_card").addClass("mandatory");
	        $("#id_card").attr("placeholder", "ID/Passport Number")
	    }
	});

  // ----------------------------------------------------validations-----------------------------------------------------

  $(".custom-combobox-input").addClass('mandatory');

  $.validator.addMethod("cRequired", function(value, element, param) {
    return !value ? false :true
    }, "Contractor name is required");

  $.validator.addClassRules("custom-combobox-input", {
      cRequired:{
        depends: function(element) {
            return $('#employee').is(':checked') || $('#contractor').is(':checked');
        }
      },
  });

	$.validator.addMethod("phone_number", function(value, element) {
	  return this.optional(element) || /^(?!-.*$)([+]?)(?!-.*$)([0-9-]){8,14}?$/.test(value);
	}, "Provide a valid phone number");

	$.validator.addMethod("extension", function(value, element, param) {
		param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif|bmp";
		return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
	});

  $("#register").validate({
      rules: {
        first_name: {
          required: true,
          maxlength: 30
        },
        last_name: {
          required: true,
          maxlength: 30
        },
        email: {
          required: true,
          email: true
        },
        phone:{
          // required: true,
          phone_number: true
          // maxlength: 15
        },
        file_text: {
          required: true,
          extension: true
        },
        id_card:{
          required: {
            depends: function(element) {
                return $('#visitor').is(':checked') || $('#employee').is(':checked')
            }
          },
          maxlength: 20
        },
        visit_datetime:{
        	required: true
        },
        employee_name:{
          required:{
            depends: function(element) {
                return $('#visitor').is(':checked')
            }
          },
          // maxlength: 30
        },
        purpose_of_visit:{
          required:{
            depends: function(element) {
                return $('#visitor').is(':checked')
            }
          },
          maxlength: 200
        },
        // company_name:{
        //   required:{
        //     depends: function(element) {
        //         return $('#contractor').is(':checked')
        //     }
        //   },
        //   maxlength: 150
        // },
        password: {
          required: true,
          minlength: 8
        },
        confirm_password: {
          required: true,
          minlength: 8,
          equalTo: "#password"
        },
        accept:{
        	required: true
        },
        area_name:{
          required: {
             depends: function(element) {
                return $('#visitor').is(':checked') || $('#employee').is(':checked')
            }
          }
        },
      },
      messages: {
        first_name: {
          required: "Please enter your first name",
          maxlength: "Maximum length of first name is 30 characters"
        },
        last_name: {
          required: "Please enter your last name",
          maxlength: "Maximum length of last name is 30 characters"
        },
        email: "Please enter a valid email address",
        // phone:{
        //   // required: "Phone is required",
        //   maxlength: "Maximum length of phone is 13 characters"
        // },
        file_text:{
          required: "Profile image is required",
          extension: "Only images are allowed"
        },
        id_card:{
          required: "ID is required",
          maxlength: "Maximum length of id card number is 20"
        },
        visit_datetime:{
        	required: "Visiting time is required"
        },
        employee_name:{
          required: "Employee name is required",
          // maxlength: "Maximum length of employee name is 30 characters"
        },
        purpose_of_visit:{
          required: "Visiting purpose is required",
          maxlength: "Maximum length of purpose is 200 characters"
        },
        // company_name:{
        //   required: "Company name is required",
        //   maxlength: "Maximum length of company name is 150 characters"
        // },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 8 characters long"
        },
        confirm_password: {
          required: "Please provide a confirm password",
          minlength: "Your password must be at least 8 characters long",
          equalTo: "Please enter the same password as previous"
        },
        accept:{
        	required: "Accept terms and conditions"
        },
        area_name:{
          required: "Area is required"
        }
      }
    })



  // -------------------------------webcam integration---------------------------------------

  var localStream;
  var is_streaming_live = false;

$("#uploadPicture").click(function(){
  	$("#user_image").click();
  });

  $("#startWebcam").click(function(){
      if(navigator.mediaDevices)
      {
        startup();
        document.getElementById('video').style = "display:block";
      
        document.getElementById('uploadPicture').style = "display:none";
        document.getElementById('startbutton').style = "display:inline-block";
        // document.getElementById('clearbutton').style = "display:inline-block";
        document.getElementById('stopWebcam').style = "display:inline-block";
      	document.getElementById('startWebcam').style = "display:none";
      }
      else{
        alert("No media device found")
      }
   });

  $("#startbutton").click(function(){
      clearphoto();
      if(is_streaming_live)
      {
        document.getElementById('video').style = "display:none";
        document.getElementById('photo').style = "display:block";
      	document.getElementById('startbutton').style = "display:none";
      	document.getElementById('clearbutton').style = "display:inline-block";
        // document.getElementById('file_text').style = "display:none";
      }
   });

   $("#clearbutton").click(function(){
      clearphoto();
      if(is_streaming_live)
      {
        document.getElementById('video').style = "display:block";
        document.getElementById('photo').style = "display:none";
      	document.getElementById('startbutton').style = "display:inline-block";
      	document.getElementById('clearbutton').style = "display:none";
        // document.getElementById('file_text').style = "display:block";
      }
   });

   $("#stopWebcam").click(function(){
      if(is_streaming_live){
        localStream.getTracks().forEach(function(track) {
          track.stop();
          is_streaming_live = track.enabled && track.readyState === 'live'? true : false;
        });

        photo.setAttribute('src', '');
        $('#webcamImg').val('');

        //add validations for file upload if image will clear from webcam image canvas
        // document.getElementById('file_text').style = "display:block";
        $('#file_text').rules('add',  { required: true });
        $('#file_text').rules('add',  { extension: true });
        $('#file_text').addClass("mandatory");

        document.getElementById('video').style = "display:none";
        document.getElementById('photo').style = "display:none";
      
        document.getElementById('uploadPicture').style = "display:inline-block";
        document.getElementById('startbutton').style = "display:none";
        document.getElementById('clearbutton').style = "display:none";
        document.getElementById('stopWebcam').style = "display:none";
        document.getElementById('startWebcam').style = "display:inline-block";
      }
   });

  var width = 320;    // We will scale the photo width to this
  var height = 0;     // This will be computed based on the input stream

  var streaming = false;

  var video = null;
  var canvas = null;
  var photo = null;
  var startbutton = null

  // startup();

  function startup() {
    video = document.getElementById('video');
    canvas = document.getElementById('canvas');
    photo = document.getElementById('photo');
    startbutton = document.getElementById('startbutton');
    if(navigator.mediaDevices)
    {
      navigator.mediaDevices.getUserMedia({ video: true, audio: false })
      .then(function(stream) {
          // localStream = stream;
          localStream = video.srcObject = stream;
          video.play();

          localStream.getTracks().forEach(function(track) {
            is_streaming_live = track.enabled && track.readyState === 'live'? true : false;
          });

      })
      .catch(function(err) {
          console.log("An error occurred: " + err);
      });

      video.addEventListener('canplay', function(ev){
        if (!streaming) {
          height = video.videoHeight / (video.videoWidth/width);
        
          video.setAttribute('width', width);
          video.setAttribute('height', height);
          canvas.setAttribute('width', width);
          canvas.setAttribute('height', height);
          streaming = true;
        }
      }, false);

      startbutton.addEventListener('click', function(ev){
        takepicture();
        ev.preventDefault();
      }, false);

      clearphoto();
    }
  }

  function clearphoto() {
    if(is_streaming_live){
      var context = canvas.getContext('2d');
      context.fillStyle = "#AAA";
      context.fillRect(0, 0, canvas.width, canvas.height);

      var data = canvas.toDataURL('image/png');
      photo.setAttribute('src', data);
      $('#webcamImg').val('');

      //add validations for file upload if image will clear from webcam image canvas
      $('#file_text').rules('add',  { required: true });
      $('#file_text').rules('add',  { extension: true });
      $('#file_text').addClass("mandatory");
    }
  }

  function takepicture() {
    if(is_streaming_live)
    {
      var context = canvas.getContext('2d');
      if (width && height) {
        canvas.width = width;
        canvas.height = height;
        context.drawImage(video, 0, 0, width, height);
      
        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
        $('#webcamImg').val(data);

        //remove validations for file upload if image will capture using webcam
        $('#file_text').rules('remove',  'required')
        $('#file_text').rules('remove',  'extension')
        $('#file_text').removeClass("mandatory");
      } else {
        clearphoto();
      }
    }
  }

  function dataURLtoFile(dataurl, filename) {
    let arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
  };

});