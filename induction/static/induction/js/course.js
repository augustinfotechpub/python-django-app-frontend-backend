$( document ).ready(function() {
	// call to get history question list
  
  $('.historyClick').click(function(){
    var history_id = $(this)[0].id;
    if(history_id){
      $.ajax({
        type: "GET",
        url: window.location.origin + "/questionhistory/" + history_id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){

            var count=0
            var html=''
            $.each(data, function (){
              count+=1
              html += '<div>';
              html += '<div><strong>Question: </strong> '+this.user_course_question__question_title+' </div>';
              html += '<div><strong>Your Answer: </strong> '+(this.user_course_actual_answer ? this.user_course_actual_answer : 'Pending')+'</div>';
              html += '<div><strong>Question Weightage: </strong> '+this.user_course_question_weightage+'</div>';
              html += '<div><strong>Obtained Weightage: </strong> '+(this.user_course_question_obtained_weightage ? this.user_course_question_obtained_weightage : '0')+'</div>';
              html += '</div>';
              if(count != data.length)
                html += '<hr>';
            });

            if(!data.length)
            {
              html += 'No information available.';
            }
            $('.modal-body').html(html);
            $('#historyModal').modal('show');
        },
        failure: function () {
          var html='<div> Failed to get questions information</div>';
          $('.modal-body').html(html);
          $('#historyModal').modal('show');
        }
      });
    }
  });

});

var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        videoId: 'TNdql6ROj2Q',
        events: {
            onReady: initialize
        }
    });
}

function initialize(){

    // Update the controls on load
    updateTimerDisplay();
    updateProgressBar();

    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    // the elapsed part of the progress bar every second.
    time_update_interval = setInterval(function () {
        updateTimerDisplay();
        updateProgressBar();
    }, 1000)

}

// This function is called by initialize()
function updateTimerDisplay(){
    // Update current time text display.
    console.log()
    $('#current-time').text(formatTime( player.getCurrentTime() ));
    $('#duration').text(formatTime( player.getDuration() ));
}

function formatTime(time){
    time = Math.round(time);

    var minutes = Math.floor(time / 60),
    seconds = time - minutes * 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;

    return minutes + ":" + seconds;
}