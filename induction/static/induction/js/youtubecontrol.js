
var videoID = $('#videoID').val();

if(is_completed == 0 && page != last_page)
{
  document.getElementById('next-button').style.display = 'none';
}

if(is_completed == 1 && page == last_page)
{
  document.getElementById('btnQuestionnaire').style.display = 'block';
}

var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        videoId: $('#youTubeVideoId').val(),
        playerVars: {
            'rel': 0,
            'controls':0
        },
        events: {
            onReady: initialize
        }       
    });
}

function initialize(){ 
  if(is_completed == 0)
  {
    // Update the controls on load
    updateTimerDisplay();

    // Clear any old interval.
    clearInterval(time_update_interval);

    // Start interval to update elapsed time display and
    var time_update_interval = setInterval(function () {
        updateTimerDisplay();
        // updateProgressBar();
    }, 1000)
  }
}

var action = 'inProgress'
// This function is called by initialize()
function updateTimerDisplay(){
    // Update current time text display.
    // $('#current-time').text(player.getCurrentTime());
    // $('#duration').text(player.getDuration());

    if(player.getDuration() == player.getCurrentTime())
    {
      if(action == 'inProgress')
      {
        // call to add completed video in history START //
        var request = new XMLHttpRequest()
        var url = window.location.origin + "/updatehistory/" + history_id.toString() +"/"+ videoID;
        request.open('GET', url, true)

        request.onload = function() {

          var data = JSON.parse(this.response)
          if (request.status >= 200 && request.status < 400) {
            // console.log(data);
            if(data.history)
            {
              //Show next button
              document.getElementById('next-button').style.display = 'block';

              //Show questionnarie button
              if(page == last_page)
              {
                document.getElementById('btnQuestionnaire').style.display = 'block';
              }
              // set action to completed to avoid multiple call for the api
              action = 'completed'
            }
          }
          else {
            console.log('error')
          }

        }

        request.send()
        // call to add completed video in history END //


        // console.log(action);
      }
    }
    // console.log(action);
}