$( document ).ready(function() {  
  $("#login").validate({
      rules: {
        username: {
          required: true,
        },
        password: {
          required: true,
          // minlength: 8
        }
      },
      messages: {
        username: {
          maxlength: "Username is required"
        },
        password: {
          required: "Please provide a password",
          // minlength: "Your password must be at least 8 characters long"
        }
      }
    })

});