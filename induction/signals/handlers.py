from uuid import uuid4
from django.core.mail import EmailMessage
from django.db.models.signals import post_save, m2m_changed
from django.db import transaction
from django.dispatch import receiver
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from induction.models.electrogasuser import AdminUser
from induction.models.contractor import Contractor
from induction.models.super_contractor import SuperContractor
from induction.models.employee import Employee
from induction.models.visitor import Visitor
from induction.models.course import Course
from induction.models.user_course import UserCourse
from induction.models.user_course_question_answer import UserCourseQuestionAnswer
from django.contrib.auth.models import User

from django.conf import settings
from induction.models.site_settings import SiteSetting

@receiver(post_save, sender=Contractor, dispatch_uid=uuid4().hex)
def save_contractor_group(sender, instance, created, **kwargs):
    if created:
    	if not Group.objects.filter(name='Contractor').exists():
        	create_group = Group(name = 'Contractor')
        	create_group.save()

        	permission = Permission.objects.get(codename='add_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='change_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='delete_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='view_employee')
        	create_group.permissions.add(permission)

        	create_group.save()
        	
        	instance.groups.add(create_group)
    	default_group = Group.objects.get(name='Contractor')
    	instance.groups.add(default_group)
    	instance.is_staff = True
    	instance.save()

@receiver(post_save, sender=SuperContractor, dispatch_uid=uuid4().hex)
def save_supercontractor_group(sender, instance, created, **kwargs):
    if created:
    	if not Group.objects.filter(name='Super Contractor').exists():
        	create_group = Group(name = 'Super Contractor')
        	create_group.save()

        	permission = Permission.objects.get(codename='add_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='change_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='delete_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='view_employee')
        	create_group.permissions.add(permission)

        	permission = Permission.objects.get(codename='view_contractor')
        	create_group.permissions.add(permission)

        	create_group.save()
    	default_group = Group.objects.get(name='Super Contractor')
    	instance.groups.add(default_group)
    	instance.is_staff = True
    	instance.save()

@receiver(post_save, sender=Employee, dispatch_uid=uuid4().hex)
def save_employee_group(sender, instance, created, **kwargs):
    if created:
    	if not Group.objects.filter(name='Employee').exists():
        	create_group = Group(name = 'Employee')
        	create_group.save()
        	instance.groups.add(create_group)
    	default_group = Group.objects.get(name='Employee')
    	instance.groups.add(default_group)

@receiver(post_save, sender=Visitor, dispatch_uid=uuid4().hex)
def save_visitor_group(sender, instance, created, **kwargs):
    if created:
    	if not Group.objects.filter(name='Visitor').exists():
        	create_group = Group(name = 'Visitor')
        	create_group.save()
        	instance.groups.add(create_group)
    	default_group = Group.objects.get(name='Visitor')
    	instance.groups.add(default_group)

# (1) Create a decorator for transaction_commit
# def on_transaction_commit(func):
#     ''' Create the decorator '''
#     def inner(*args, **kwargs):
#         transaction.on_commit(lambda: func(*args, **kwargs))

#     return inner
# (2) Alternative for m2m changed signal example
# @receiver(post_save, sender=ElectrogasUser, dispatch_uid=uuid4().hex)
# @on_transaction_commit
# def save_user_course_history_of_electrogas_user_m2m_alter(sender, instance, created, **kwargs):
# 	pass
# Commented 


# @receiver(post_save, sender=User, dispatch_uid=uuid4().hex)
# def send_mail_to_user_while_registration(sender, instance, created, **kwargs):
# 	if created:
# 		email_body = """\
# 	    <html>
# 	      <head><title>Electrogas Email</title></head>
# 	      <body>
# 	        <h2>Welcome to electrogas</h2>
# 	        <p>Hello %s,</p>
# 	        <p>You have just registered successfully</p>
# 	        <p>Thanks</p>
# 	      </body>
# 	    </html>
# 	    """ % (instance.username)
# 		email = EmailMessage('Electrogas Registration!', email_body, to=[instance.email])
# 		email.content_subtype = "html"
# 		email.send(fail_silently=True)
# 		send_mail_to_administrator(instance)

@receiver(post_save, sender=AdminUser, dispatch_uid=uuid4().hex)
def send_mail_to_electrogasuser_while_registration(sender, instance, created, **kwargs):
	if created and instance.email:
		auto_assigned_course_list = Course.objects.filter(course_auto_assigned=True, course_status='Active')
		if auto_assigned_course_list:
			if len(auto_assigned_course_list):
				for c in auto_assigned_course_list:
					course_to_assign = Course.objects.get(pk=c.id)
					instance.user_course.add(course_to_assign)

		email_body = """\
	    <html>
	      <head><title>Electrogas Email</title></head>
	      <body>
	        <h2>Welcome to electrogas</h2>
	        <p>Hello %s,</p>
	        <p>You have successfully created a user !</p>
	        <p>Thanks</p>
	      </body>
	    </html>
	    """ % (instance.username)
		email = EmailMessage('Electrogas Registration!', email_body, to=[instance.email])
		email.content_subtype = "html"
		email.send(fail_silently=True)
		send_mail_to_administrator(instance)

@receiver(post_save, sender=Contractor, dispatch_uid=uuid4().hex)
def send_mail_to_contactor_while_registration(sender, instance, created, **kwargs):
	if created:
		auto_assigned_course_list = Course.objects.filter(course_auto_assigned=True, course_status='Active')
		if auto_assigned_course_list:
			if len(auto_assigned_course_list):
				for c in auto_assigned_course_list:
					course_to_assign = Course.objects.get(pk=c.id)
					instance.contractor_course.add(course_to_assign)

		email_body = """\
	    <html>
	      <head><title>Electrogas Email</title></head>
	      <body>
	        <h2>Welcome to electrogas</h2>
	        <p>Hello %s,</p>
	        <p>You have just registered successfully</p>
	        <p>Thanks</p>
	      </body>
	    </html>
	    """ % (instance.username)
		email = EmailMessage('Electrogas Registration!', email_body, to=[instance.email])
		email.content_subtype = "html"
		email.send(fail_silently=True)
		send_mail_to_administrator(instance)

@receiver(post_save, sender=SuperContractor, dispatch_uid=uuid4().hex)
def send_mail_to_supercontactor_while_registration(sender, instance, created, **kwargs):
	if created:
		email_body = """\
	    <html>
	      <head><title>Electrogas Email</title></head>
	      <body>
	        <h2>Welcome to electrogas</h2>
	        <p>Hello %s,</p>
	        <p>You have just registered successfully</p>
	        <p>Thanks</p>
	      </body>
	    </html>
	    """ % (instance.username)
		email = EmailMessage('Electrogas Registration!', email_body, to=[instance.email])
		email.content_subtype = "html"
		email.send(fail_silently=True)
		send_mail_to_administrator(instance)

@receiver(post_save, sender=Employee, dispatch_uid=uuid4().hex)
def send_mail_to_employee_while_registration(sender, instance, created, **kwargs):
	if created:
		email_body = """\
	    <html>
	      <head><title>Electrogas Email</title></head>
	      <body>
	        <h2>Welcome to electrogas</h2>
	        <p>Hello %s,</p>
	        <p>You have just registered successfully</p>
	        <p>Thanks</p>
	      </body>
	    </html>
	    """ % (instance.username)
		email = EmailMessage('Electrogas Registration!', email_body, to=[instance.email])
		email.content_subtype = "html"
		email.send(fail_silently=True)
		send_mail_to_administrator(instance)

@receiver(post_save, sender=Visitor, dispatch_uid=uuid4().hex)
def send_mail_to_visitor_while_registration(sender, instance, created, **kwargs):
	if created:
		auto_assigned_course_list = Course.objects.filter(course_auto_assigned=True, course_status='Active')
		if auto_assigned_course_list:
			if len(auto_assigned_course_list):
				for c in auto_assigned_course_list:
					course_to_assign = Course.objects.get(pk=c.id)
					instance.visitor_course.add(course_to_assign)

		email_body = """\
	    <html>
	      <head><title>Electrogas Email</title></head>
	      <body>
	        <h2>Welcome to electrogas</h2>
	        <p>Hello %s,</p>
	        <p>You have just registered successfully</p>
	        <p>Thanks</p>
	      </body>
	    </html>
	    """ % (instance.username)
		email = EmailMessage('Electrogas Registration!', email_body, to=[instance.email])
		email.content_subtype = "html"
		email.send(fail_silently=False)
		send_mail_to_administrator(instance)

def send_mail_to_administrator(instance):
	site = SiteSetting.objects.get(site_name=settings.SITE_NAME)
	email_body = """\
    <html>
      <head><title>User Registration Information</title></head>
      <body>
        <h2>New User Registered</h2>
        <p>Hello Administrator,</p>
        <p>New user %s has just registered with email address %s </p>
        <p>Thanks</p>
      </body>
    </html>
    """ % (instance.username, instance.email)
	email = EmailMessage('Electrogas Registration!', email_body, to=[site.admin_email])
	email.content_subtype = "html"
	email.send(fail_silently=True)	