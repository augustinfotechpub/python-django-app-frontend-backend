from django import forms
from induction.models.video import Video
from induction.models.question import Question
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple

class VideoForm(forms.ModelForm):
    video_question = forms.ModelMultipleChoiceField(
          queryset=Question.objects.all().filter(question_status='Active'),
          required=True,
          widget=FilteredSelectMultiple(
              verbose_name=_('Questions'),
              is_stacked=False
          ),
          label=_('Select Questions')
    )

    def __init__(self, *args, **kwargs):
        super(VideoForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.fields['video_question'].initial = self.instance.video_question.all().filter(question_status='Active')

    class Meta:
        model = Video
        fields = '__all__'
			
