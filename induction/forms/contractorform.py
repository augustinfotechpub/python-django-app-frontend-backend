from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from induction.models.contractor import Contractor
from induction.models.course import Course
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class ContractorChangeForm(UserChangeForm):
	username = forms.CharField(required=True, min_length=5)
	contractor_course = forms.ModelMultipleChoiceField(
          queryset=Course.objects.all().filter(course_status='Active'),
          required=False,
          widget=FilteredSelectMultiple(
              verbose_name=_('Courses'),
              is_stacked=False
          ),
          label=_('Select Courses')
    )

	def __init__(self, *args, **kwargs):
		super(ContractorChangeForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		if self.instance and self.instance.pk:
			self.fields['contractor_course'].initial = self.instance.contractor_course.all().filter(course_status='Active')

	class Meta:
		model = Contractor
		fields = '__all__'

class ContractorCreationForm(UserCreationForm):
	username = forms.CharField(required=True, min_length=5)
	def __init__(self, *args, **kwargs):
		super(ContractorCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True

	def clean_email(self):
		email = self.cleaned_data['email']
		if User.objects.filter(email=email).exists():
			raise ValidationError("Email already exists")
		return email

	class Meta:
		model = Contractor
		fields = '__all__'	
			
