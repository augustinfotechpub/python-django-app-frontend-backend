from django import forms
from induction.models.question import Question
from django.core.exceptions import ValidationError
from django.forms.models import BaseInlineFormSet

class QuestionFormInlineOptionValidation(BaseInlineFormSet):
    def clean(self):
        super(QuestionFormInlineOptionValidation, self).clean()
        non_empty_forms = 0
        for form in self:
            if form.cleaned_data:
                non_empty_forms += 1
        if non_empty_forms - len(self.deleted_forms) < 2:
            raise ValidationError("Please add at least 2 options.")

        answers = 0
        for form in self:
        	if form.cleaned_data:
        		if form.cleaned_data.get('option_is_answer'):
        			answers += 1
       	if not answers:
       		raise ValidationError("Please choose at least one option as an answer.")
       	elif answers > 1:
       		raise ValidationError("You can't choose multiple options as an answer.")

			
