from django import forms

class HistoryForm(forms.Form):
	ALL='All'
	PASS='Pass'
	FAIL='Fail'
	PENDING='Pending'

	COMPLETED = 'Completed'
	ATTEMPTED = 'Attempted'
	NOT_ATTEMPTED = 'Not Attempted'

	RESULT_CHOICES = ((ALL,'All'),(PASS,'Pass'),(FAIL,'Fail'),(PENDING,'Pending'))
	STATUS_CHOICES = ((ALL,'All'),(COMPLETED,'Completed'),(ATTEMPTED,'Attempted'),(NOT_ATTEMPTED,'Not Attempted'))
	course_complete_date_from = forms.CharField(label="From Complete Date", required=False)
	course_complete_date_to = forms.CharField(label="To Complete Date", required=False)
	status = forms.ChoiceField(label="Status", choices=STATUS_CHOICES, required=False)
	result = forms.ChoiceField(label="Result", choices=RESULT_CHOICES, required=False)
	course = forms.CharField(label="Course", required=False)
	first_name = forms.CharField(label="First Name", required=False)
	last_name = forms.CharField(label="Last Name", required=False)
	contractor = forms.CharField(label="Contractor", required=False)

			
