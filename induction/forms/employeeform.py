from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from induction.models.employee import Employee
from induction.models.contractor import Contractor
from induction.models.course import Course
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class EmployeeChangeForm(UserChangeForm):
	# employee_course = forms.ModelMultipleChoiceField(
 #          queryset=Course.objects.all(),
 #          required=False,
 #          widget=FilteredSelectMultiple(
 #              verbose_name=_('Courses'),
 #              is_stacked=False
 #          ),
 #          label=_('Select Courses')
 #    )
 	username = forms.CharField(required=True, min_length=5)
 	def __init__(self, *args, **kwargs):
 		super(EmployeeChangeForm, self).__init__(*args, **kwargs)
 		self.fields['email'].required = True
 		if self.current_user.groups.all().filter(name='Contractor').exists():
 			self.fields['employee_contractor'].widget = forms.HiddenInput()
 			self.fields['employee_contractor'].initial = self.current_user.id
 	class Meta:
 		model = Employee
 		fields = '__all__'

class EmployeeCreationForm(UserCreationForm):
	username = forms.CharField(required=True, min_length=5)
	def __init__(self, *args, **kwargs):
		# self.groups = 'Employee'
		super(EmployeeCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		if self.current_user.groups.all().filter(name='Contractor').exists():
			self.fields['employee_contractor'].widget = forms.HiddenInput()
			self.fields['employee_contractor'].initial = self.current_user.id

	def clean_email(self):
 		email = self.cleaned_data['email']
 		if User.objects.filter(email=email).exists():
 			raise ValidationError("Email already exists")
 		return email
	class Meta:
		model = Employee
		fields = '__all__'
			
