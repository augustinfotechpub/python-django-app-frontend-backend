from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from induction.models.visitor import Visitor
from tinymce.widgets import TinyMCE
from induction.models.course import Course
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class VisitorChangeForm(UserChangeForm):
	username = forms.CharField(required=True, min_length=5)
	visitor_course = forms.ModelMultipleChoiceField(
          queryset=Course.objects.all().filter(course_status='Active'),
          required=False,
          widget=FilteredSelectMultiple(
              verbose_name=_('Courses'),
              is_stacked=False
          ),
          label=_('Select Courses')
    )

	def __init__(self, *args, **kwargs):
		super(VisitorChangeForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		if self.instance and self.instance.pk:
			self.fields['visitor_course'].initial = self.instance.visitor_course.all().filter(course_status='Active')

	visitor_purpose_of_visit = forms.CharField(widget=TinyMCE(attrs={'cols': 40, 'rows': 10}), label='Purpose of visit', required=False)
	class Meta:
		model = Visitor
		fields = '__all__'

class VisitorCreationForm(UserCreationForm):
	username = forms.CharField(required=True, min_length=5)
	def __init__(self, *args, **kwargs):
		super(VisitorCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True

	visitor_purpose_of_visit = forms.CharField(widget=TinyMCE(attrs={'cols': 40, 'rows': 10}), label='Purpose of visit', required=False)

	def clean_email(self):
		email = self.cleaned_data['email']
		if User.objects.filter(email=email).exists():
			raise ValidationError("Email already exists")
		return email

	class Meta:
		model = Visitor
		fields = '__all__'
			
