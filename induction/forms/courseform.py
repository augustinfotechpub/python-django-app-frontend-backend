from django import forms
from induction.models.video import Video
from induction.models.course import Course
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from tinymce.widgets import TinyMCE

class CourseForm(forms.ModelForm):
    course_video = forms.ModelMultipleChoiceField(
          queryset=Video.objects.all().filter(video_status='Active'),
          required=True,
          widget=FilteredSelectMultiple(
              verbose_name=_('Videos'),
              is_stacked=False
          ),
          label=_('Select Videos')
    )

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.fields['course_video'].initial = self.instance.course_video.all().filter(video_status='Active')

    course_description = forms.CharField(widget=TinyMCE(attrs={'cols': 65, 'rows': 15}), label='Description')
    class Meta:
        model = Course
        fields = '__all__'
			
