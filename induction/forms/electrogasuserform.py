from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from induction.models.electrogasuser import AdminUser
from tinymce.widgets import TinyMCE
from induction.models.course import Course
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class ElectrogasUserChangeForm(UserChangeForm):
	username = forms.CharField(required=True, min_length=5)
	# get_restricted_perm_list = ['logentry','group','permission','user','contenttype','session']
	# content_id_list = []
	# for c in get_restricted_perm_list:
	# 	content = ContentType.objects.get(model=c)
	# 	content_id_list.append(content.id)
	# print(content_id_list)
	# groups = forms.ModelMultipleChoiceField(
 #          queryset=Group.objects.filter().exclude(name__in = ["Contractor","Employee","Visitor"]),
 #          required=False,
 #          widget=FilteredSelectMultiple(
 #              verbose_name=_('Group'),
 #              is_stacked=False
 #          ),
 #          label=_('Select Group')
 #    )
 	
	# [1,2,3,5,6] [12,13,14,16,17] [13,14,15,17,18]
	user_permissions = forms.ModelMultipleChoiceField(
          queryset=Permission.objects.filter().exclude(content_type_id__in = [13,14,15,17,18]),
          required=False,
          widget=FilteredSelectMultiple(
              verbose_name=_('Persmission'),
              is_stacked=False
          ),
          label=_('Select Permission')
    )
	user_course = forms.ModelMultipleChoiceField(
          queryset=Course.objects.all().filter(course_status='Active'),
          required=False,
          widget=FilteredSelectMultiple(
              verbose_name=_('Courses'),
              is_stacked=False
          ),
          label=_('Select Courses')
    )

	def __init__(self, *args, **kwargs):
		super(ElectrogasUserChangeForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		if self.instance and self.instance.pk:
			self.fields['user_course'].initial = self.instance.user_course.all().filter(course_status='Active')
			
	class Meta:
		model = AdminUser
		fields = '__all__'

class ElectrogasUserCreationForm(UserCreationForm):
	username = forms.CharField(required=True, min_length=5)
	def __init__(self, *args, **kwargs):
		super(ElectrogasUserCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
	def clean_email(self):
 		email = self.cleaned_data['email']
 		if User.objects.filter(email=email).exists():
 			raise ValidationError("Email already exists")
 		return email
	class Meta:
		model = AdminUser
		fields = '__all__'
			
