from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from induction.models.super_contractor import SuperContractor
from induction.models.contractor import Contractor
from django.utils.translation import gettext as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class CustomModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
         return obj.username

class SuperContractorChangeForm(UserChangeForm):
	username = forms.CharField(required=True, min_length=5)
	sc_contractor = CustomModelMultipleChoiceField(
          queryset=Contractor.objects.all().filter(is_active=True, contractor_is_main=True),
          required=False,
          widget=FilteredSelectMultiple(
              verbose_name=_('Contractors'),
              is_stacked=False
          ),
          label=_('Select Contractors')
    )

	def __init__(self, *args, **kwargs):
		super(SuperContractorChangeForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		if self.instance and self.instance.pk:
			self.fields['sc_contractor'].initial = self.instance.sc_contractor.all().filter(is_active=True, contractor_is_main=True)

	class Meta:
		model = SuperContractor
		fields = '__all__'

class SuperContractorCreationForm(UserCreationForm):
	username = forms.CharField(required=True, min_length=5)
	def __init__(self, *args, **kwargs):
		super(SuperContractorCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True

	def clean_email(self):
		email = self.cleaned_data['email']
		if User.objects.filter(email=email).exists():
			raise ValidationError("Email already exists")
		return email

	class Meta:
		model = SuperContractor
		fields = '__all__'	
			
