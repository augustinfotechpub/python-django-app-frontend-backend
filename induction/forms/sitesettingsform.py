from django import forms
from induction.models.site_settings import SiteSetting
from tinymce.widgets import TinyMCE

class SiteSettingsForm(forms.ModelForm):
    welcome_note = forms.CharField(widget=TinyMCE(attrs={'cols': 65, 'rows': 15}), label='Welcome Note')
    course_list_note = forms.CharField(widget=TinyMCE(attrs={'cols': 65, 'rows': 15}), label='Note about Course list')
    
    class Meta:
        model = SiteSetting
        fields = '__all__'
			
