from django import template
import urllib.parse as urlparse
from urllib.parse import parse_qs

register = template.Library()

@register.filter
def get_youtube_video_id(value):
    parsed = urlparse.urlparse(value)
    if parse_qs(parsed.query):
    	if 'v' in parse_qs(parsed.query).keys():
    		return parse_qs(parsed.query)['v'][0]
    	else:
    		return value
    else:
    	return value