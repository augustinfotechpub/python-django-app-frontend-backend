"""chart_demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path, re_path
from . import views
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'induction'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('terms/', views.terms, name='terms'),
    path('logout/', views.logout, name='logout'),
    path('course/', views.course, name='course'),
    path('course/<course_id>', views.course_detail, name='course_detail'),
    path('gotoquestion/', views.go_to_question, name='go_to_question'),
    path('question/', views.question, name='question'),
    path('certificate/<history_id>', views.certificate, name='certificate'),
    path('result/<history_id>', views.result, name='result'),
    path('history/', views.course_history, name='history'),
    path('profile/update/', views.update_profile, name='profile-update'),
    path('update-password/', views.update_password, name='password_update'),
    path('contractors/', views.contractor_list_by_company_and_id, name='contractor_list_by_company_and_id'),
    path('areas/', views.area_list, name='area_list'),
    path('employee/<area_id>', views.employee_list_by_area, name='employee_list_by_area'),
    path('user/<username>/', views.is_user_exist, name='is_user_exist'),
    path('contractor/<company_name>/<id_card>', views.is_contractor_exist, name='is_contractor_exist'),
    path('checkid/<id_card>', views.is_id_card_exist, name='is_employee_visitor_exist'),
    path('questionhistory/<history_id>', views.get_history_questions_list, name='questionhistory'),
    path('updatehistory/<history_id>/<video_id>', views.update_completed_video, name='update-history'),

    # Password reset urls
    path('reset-password/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('reset-password/done', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset-password/confirm/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset-password/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('email/', views.send_certificate_mail, name='certificate_mail'),
]

# if settings.DEBUG:
#     urlpatterns += static(settings.USER_IMAGE_URL, document_root=settings.USER_IMAGE_DIR)

