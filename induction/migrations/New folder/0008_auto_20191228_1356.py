# Generated by Django 2.2.8 on 2019-12-28 08:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('induction', '0007_auto_20191228_1350'),
    ]

    operations = [
        migrations.RenameField(
            model_name='visitor',
            old_name='visitror_course',
            new_name='visitor_course',
        ),
    ]
