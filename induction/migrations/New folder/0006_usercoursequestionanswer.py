# Generated by Django 2.2.8 on 2019-12-27 13:55

from django.db import migrations, models
import django.db.models.deletion
import induction.models.custom_fields


class Migration(migrations.Migration):

    dependencies = [
        ('induction', '0005_auto_20191227_1324'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserCourseQuestionAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_course_answer', models.CharField(max_length=100)),
                ('user_course_actual_answer', models.CharField(max_length=100)),
                ('user_course_question_weightage', induction.models.custom_fields.PositiveFloatField(verbose_name='Total weightage')),
                ('user_course_question_obtained_weightage', induction.models.custom_fields.PositiveFloatField(verbose_name='Obtained weightage')),
                ('user_course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='induction.UserCourse', verbose_name='User course')),
                ('user_course_question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='induction.Question', verbose_name='Course question')),
            ],
        ),
    ]
