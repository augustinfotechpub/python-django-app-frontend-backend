from django.contrib import messages
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.shortcuts import get_list_or_404, get_object_or_404
from django.conf import settings
from induction.models.site_settings import SiteSetting
from induction.views.login import get_user_session, model_to_dict
from induction.views.api import get_user_course_history, get_current_time_as_setting_timezone
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from induction.models.video import Video
from induction.models.visitor import Visitor
from induction.models.contractor import Contractor
from induction.models.employee import Employee
from induction.models.electrogasuser import AdminUser
from induction.models.course import Course
from induction.models.user_course import UserCourse
from django.core.paginator import Paginator
import re
from operator import itemgetter

regexDigit = '^[0-9]+$'
# Create your views here.
def course(request):
	user_session = get_user_session(request)
	if user_session:
		course_list = get_course_list(user_session)
		site = SiteSetting.objects.get(site_name=settings.SITE_NAME)
		return render(request, 'induction/course_list.html', {'user_session':user_session, 'course_list':course_list, 'site':site})
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def course_detail(request, course_id):
	user_session = get_user_session(request)

	if user_session:
		course_list_id = list(map(itemgetter('id'), get_course_list(user_session)))

		if course_id.isnumeric():

			if int(course_id) in course_list_id:
				course = get_object_or_404(Course, pk=course_id)

				if course is not None:
					# create user course history or get existing with status.
					history_status = get_user_course_history(user_session, course_id)
					if history_status['action'] == 'Completed':
						# redirect to certificate page ditrectly if course has already completed and having active certificate
						return HttpResponseRedirect(reverse('induction:certificate', kwargs={'history_id':history_status['history_id']}))

					course_dict = model_to_dict(course)
					course_dict['course_id'] = course_id
					course_dict['history_id'] = history_status['history_id']
					request.session['course'] = course_dict

				video_list = get_video_by_history(history_status['history_id'])
				current_page = get_current_position(history_status['history_id'])
				last_page = get_last_position(history_status['history_id'])
				
				paginator = Paginator(video_list, 1)
				request_get_page = request.GET.get('page')

				# don't allow user to browse with invalid query string values
				if re.search(regexDigit, str(request.GET.get('page'))):
					# restrict user to 1 if value is negative
					if int(request.GET.get('page')) < 1:
						request_get_page = 1
					# restrict user to max length of page if value is greater than last page
					elif int(request.GET.get('page')) > last_page:
						request_get_page = last_page
				else:
					# value will be None if query string value is not digit
					request_get_page = None
				
				# redirect to the current occurence of course if request_page value is None
				page = request_get_page if request_get_page else (current_page if current_page<= last_page else last_page)

				# Set page to the last occurence page again if user will try to access the next video manually by url
				page = page if int(page) <= int(current_page) else current_page

				video_list = paginator.get_page(page)

				is_video_completed = 1 if int(page) < int(current_page) else 0

				return render(request, 'induction/course_detail.html', {'user_session':user_session, 'course': course, 'video_list': video_list, 'page':page, 'is_completed': is_video_completed, 'current_page': current_page, 'last_page':last_page, 'history_id':history_status['history_id'] })

			else:
				messages.success(request, 'The course that you have clicked is no longer available. You are allowed only following courses.')
				return HttpResponseRedirect(reverse('induction:course'))

		return HttpResponseRedirect(reverse('induction:course'))
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def get_course_list(user_session):
	if len(user_session['groups']):
		user_group = Group.objects.get(pk=user_session['groups'][0])
	else:
		user_group = None

	course_list = []
	if user_group:
		if user_group.name == 'Visitor':
			visitor_course_list = Visitor.objects.get(username=user_session['username']).visitor_course.all().filter(course_status='Active')
			for course in visitor_course_list:
				course_detail = {}
				course_detail['id'] = course.id
				course_detail['name'] = course.course_title
				course_list.append(course_detail)
		elif user_group.name == 'Contractor':
			contractor_course_list = Contractor.objects.get(username=user_session['username']).contractor_course.all().filter(course_status='Active')
			for course in contractor_course_list:
				course_detail = {}
				course_detail['id'] = course.id
				course_detail['name'] = course.course_title
				course_list.append(course_detail)
		elif user_group.name == 'Employee':
			employee_contractor_id = Employee.objects.get(username=user_session['username']).employee_contractor.id
			contractor_course_list = Contractor.objects.get(pk=employee_contractor_id).contractor_course.all().filter(course_status='Active')
			for course in contractor_course_list:
				course_detail = {}
				course_detail['id'] = course.id
				course_detail['name'] = course.course_title
				course_list.append(course_detail)
	else:
		user_course_list = AdminUser.objects.get(username=user_session['username']).user_course.all().filter(course_status='Active')
		for course in user_course_list:
			course_detail = {}
			course_detail['id'] = course.id
			course_detail['name'] = course.course_title
			course_list.append(course_detail)

	return course_list

def result(request, history_id):
	user_session = get_user_session(request)
	if user_session:
		user_course_history = get_object_or_404(UserCourse, pk=history_id)
		if str(user_session['username']) == str(user_course_history.user.username):
			if user_course_history.user_course_status == 'Completed':
				return render(request, 'induction/result.html', {'history_data':user_course_history, 'user_session':user_session})
		return HttpResponseRedirect(reverse('induction:course'))
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def certificate(request, history_id):
	user_session = get_user_session(request)
	if user_session:
		user_course_history = get_object_or_404(UserCourse, pk=history_id)
		if str(user_session['username']) == str(user_course_history.user.username) and user_course_history.user_course_result == 'Pass':
			return render(request, 'induction/certificate.html', {'history_data':user_course_history, 'user_session':user_session})
		else:
			return HttpResponseRedirect(reverse('induction:course'))
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def get_video_by_history(history_id):
	video_list = Video.objects.all().filter(id__in=UserCourse.objects.get(pk=history_id).user_course_all_videos.strip('][').split(', '))
	return video_list

def get_current_position(history_id):
	history = UserCourse.objects.get(pk=history_id)
	if history.user_course_completed_videos:
		completed_videos = history.user_course_completed_videos.strip('][').split(',')
		current_video_page = len(completed_videos) + 1
		return current_video_page
	else:
		return 1

def get_last_position(history_id):
	history = UserCourse.objects.get(pk=history_id)
	if history.user_course_all_videos:
		all_video = history.user_course_all_videos.strip('][').split(',')
		last_video_page = len(all_video)
		return last_video_page
	else:
		return 0

def course_history(request):
	user_session = get_user_session(request)
	if user_session:
		try:
			history_list = get_list_or_404(UserCourse.objects.order_by('-user_course_attempt_date'), user__username=user_session['username'])
		except:
			history_list = None
		return render(request, 'induction/course_history.html', {'history_list':history_list, 'user_session':user_session})
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def get_course_session(request):
	course_session = None
	if "course" in request.session:
		if request.session['course']:
			course_session = request.session['course']

	return course_session
