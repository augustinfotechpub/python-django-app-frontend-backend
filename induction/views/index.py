from django.contrib import messages
from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.conf import settings
from induction.models.site_settings import SiteSetting
from induction.views.login import get_user_session
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from induction.models.visitor import Visitor
from induction.models.contractor import Contractor
from induction.models.employee import Employee
from induction.models.electrogasuser import AdminUser
from induction.views.login import model_to_dict
import re

regexPhone = '^(?!-.*$)([+]?)(?!-.*$)([0-9-]){8,14}?$'

# Create your views here.
def index(request):
	user_session = get_user_session(request)
	if user_session:
		site = SiteSetting.objects.get(site_name=settings.SITE_NAME)
		return render(request, 'induction/index.html', {'user_session':user_session, 'site':site})
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def update_password(request):
	user_session = get_user_session(request)
	if user_session:
		user = User.objects.get(username=user_session['username'])
		if request.method == 'POST':
			form = PasswordChangeForm(user, request.POST)
			if form.is_valid():
				user = form.save()
				update_session_auth_hash(request, user)  # Important!
				messages.success(request, 'Your password successfully updated!')
				return HttpResponseRedirect(reverse('induction:password_update'))
			else:
				pass
		else:
			form = PasswordChangeForm(user)
		return render(request, 'induction/update_password.html', {'form': form, 'user_session':user_session})
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def update_profile(request):
	user_session = get_user_session(request)

	if user_session:
		user=None
		user_group = None
		if len(user_session['groups']):
			user_group = Group.objects.get(pk=user_session['groups'][0])
		else:
			user_group = None

		if request.method == 'POST':
			msg=''
			if user_group:
				if user_group.name == 'Visitor':
					try:
						user = Visitor.objects.get(username=user_session['username'])

						msg=validate_profile_form(request, 'Visitor')
						
						if len(msg):
							return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name, 'msg':msg})

						user.first_name = request.POST.get('first_name', False)
						user.last_name = request.POST.get('last_name', False)
						user.visitor_phone_number = request.POST.get('phone', False)
						user.visitor_id_card_number = request.POST.get('id_card', False)
						user.save()

						user = User.objects.get(username=user_session['username'])
						request.session['user'] = model_to_dict(user)
					except:
						msg = 'Something went wrong, check if you provide a valid details or not'
						return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name, 'msg':msg})
				elif user_group.name == 'Contractor':
					try:
						user = Contractor.objects.get(username=user_session['username'])

						msg=validate_profile_form(request, 'Contractor')
						if len(msg):
							return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name, 'msg':msg})

						user.first_name = request.POST.get('first_name', False)
						user.last_name = request.POST.get('last_name', False)
						user.contractor_phone_number = request.POST.get('phone', False)
						user.contractor_id_card_number = request.POST.get('id_card', False)
						user.save()

						user = User.objects.get(username=user_session['username'])
						request.session['user'] = model_to_dict(user)
					except:
						msg = 'Something went wrong, check if you provide a valid details or not'
						return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name, 'msg':msg})
				elif user_group.name == 'Employee':
					try:
						user = Employee.objects.get(username=user_session['username'])

						msg=validate_profile_form(request, 'Employee')
						if len(msg):
							return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name, 'msg':msg})

						user.first_name = request.POST.get('first_name', False)
						user.last_name = request.POST.get('last_name', False)
						user.employee_phone_number = request.POST.get('phone', False)
						user.employee_id_card_number = request.POST.get('id_card', False)
						user.save()

						user = User.objects.get(username=user_session['username'])
						request.session['user'] = model_to_dict(user)
					except:
						msg = 'Something went wrong, check if you provide a valid details or not'
						return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name, 'msg':msg})

			else:
				try:
					user = AdminUser.objects.get(username=user_session['username'])

					msg=validate_profile_form(request, user_group)
					if len(msg):
						return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name if user_group else None, 'msg':msg})

					user.first_name = request.POST.get('first_name', False)
					user.last_name = request.POST.get('last_name', False)
					user.user_phone_number = request.POST.get('phone', False)
					user.user_id_card_number = request.POST.get('id_card', False)
					user.save()

					user = User.objects.get(username=user_session['username'])
					request.session['user'] = model_to_dict(user)
				except:
					msg = 'Something went wrong, check if you provide a valid details or not'
					return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name if user_group else None, 'msg':msg})

			messages.success(request, 'Changes has been updated successfully!')
			return HttpResponseRedirect(reverse('induction:profile-update'))
		else:
			if user_group:
				if user_group.name == 'Visitor':
					user = Visitor.objects.get(username=user_session['username'])
				elif user_group.name == 'Contractor':
					user = Contractor.objects.get(username=user_session['username'])
				elif user_group.name == 'Employee':
					user = Employee.objects.get(username=user_session['username'])
			else:
				user = AdminUser.objects.get(username=user_session['username'])

			return render(request, 'induction/profile.html', {'user_session':user_session, 'user':user, 'user_group':user_group.name if user_group else None})

	else:
		return HttpResponseRedirect(reverse('induction:login'))

def validate_profile_form(request, user_type):
	msg = ''
	msg_count = 0
	first_name = request.POST.get('first_name', False)
	last_name = request.POST.get('last_name', False)
	phone = request.POST.get('phone', False)
	id_card = request.POST.get('id_card', False)
	company_name = request.POST.get('company_name', False)

	if not first_name or not last_name:
		msg_count+=1
		msg += '<li>Both first name and last name are required</li>'
	if len(first_name) > 30 or len(last_name) > 30:
		msg_count+=1
		msg += '<li>Maximum length of first name and last name should be 30</li>'
	if phone:
		if not re.search(regexPhone,phone):
			msg_count+=1
			msg += '<li>Please provide a valid phone number</li>'
	if not id_card or len(id_card) > 20:
		if user_type != 'Contractor':
			msg_count+=1
			msg += '<li>ID is required, maximum length should be 20</li>'
	elif id_card:
		is_exist = False
		if Visitor.objects.filter(visitor_id_card_number=id_card).exists():
			is_exist = True
		elif Employee.objects.filter(employee_id_card_number=id_card).exists():
			is_exist = True
		elif Contractor.objects.filter(contractor_id_card_number=id_card).exists():
			is_exist = True

		if is_exist:
			msg_count+=1
			msg += '<li>ID already exist.</li>'

	if user_type == 'Contractor':
		if company_name and id_card:
			if Contractor.objects.filter(contractor_company_name=company_name, contractor_id_card_number=id_card).exists():
				msg_count+=1
				msg += '<li>Id with given company name is already registered</li>'

	if msg_count > 1:
		msg = '<ul>'+ msg +'</ul>'

	return msg

def handler404(request, exception):
    return render(request, 'induction/404.html', status=404)