from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.conf import settings
from django.core import serializers
from django.contrib.auth import authenticate

from django.contrib.auth.models import User
from induction.models.site_settings import SiteSetting

import json

# Create your views here.
def login(request):
	if request.method == 'POST':
		msg=''
		username = request.POST.get('username', False)
		password = request.POST.get('password', False)
		msg = validate_login_form(username, password)
		if len(msg):
			return render(request, 'induction/login.html', {'msg':msg})

		if User.objects.filter(email=username).exists():
			username = User.objects.get(email=username).username
			
		if User.objects.filter(username=username).exists():
			if User.objects.get(username=username).is_active:
				user = authenticate(request, username=username, password=password)
				if user is not None:
					if user.groups.all().filter(name='Super Contractor').exists():
						msg = "Super contractor authentication is not allowed."
					else:
						request.session['user'] = model_to_dict(user)
						return HttpResponseRedirect(reverse('induction:index'))
				else:
					msg="Authentication failed, please login again !"
			else:
				site = SiteSetting.objects.get(site_name=settings.SITE_NAME)
				msg = "User is not activated yet.<br> Contact admin on <b>"+ site.admin_contact +"</b> or do login after sometime !"
		else:
			msg = "User does not exist, please register."

		if msg:
			return render(request, 'induction/login.html',{"msg":msg})
		else:
			return render(request, 'induction/login.html')
	else:
		user_session = get_user_session(request)
		if user_session:
			return HttpResponseRedirect(reverse('induction:index'))
		else:
			return render(request, 'induction/login.html')

def model_to_dict(instance):
	return json.loads(serializers.serialize('json', [instance]))[0]['fields']

def terms(request):
	return render(request, 'induction/terms.html')
	
def get_user_session(request):
	user_session = None
	if "user" in request.session:
		if request.session['user']:
			user_session = request.session['user']

	return user_session

def validate_login_form(username, password):
	msg=""
	msg_count = 0

	if not len(username):
		msg_count+=1
		msg += ' | ' if msg_count > 1 else ''
		msg += 'Username is required'

	if not len(password):
		msg_count+=1
		msg += ' | ' if msg_count > 1 else ''
		msg += 'Password is required'

	return msg