from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.http import JsonResponse
from django.urls import reverse
import datetime, pytz
from django.core.mail import EmailMessage
from django.contrib import messages
from induction.views.login import get_user_session
from django.contrib.auth.models import User
from induction.models.contractor import Contractor
from induction.models.visitor import Visitor
from induction.models.employee import Employee
from induction.models.course import Course
from induction.models.user_course import UserCourse
from induction.models.user_course_question_answer import UserCourseQuestionAnswer
import os
from django.conf import settings
from induction.models.area import Area

def contractor_list_by_company_and_id(request):
	user_col = ["username", "contractor_company_name", "contractor_id_card_number", "contractor_is_main"]
	return JsonResponse(list(Contractor.objects.all().filter(is_active=True).values(*user_col)), safe=False)

def area_list(request):
	user_col = ["id","area_name"]
	return JsonResponse(list(Area.objects.all().values(*user_col)), safe=False)

def employee_list_by_area(request, area_id):
	user_col = ["id","first_name", "last_name", "username"]
	return JsonResponse(list(Employee.objects.all().filter(employee_area=area_id).order_by('username').values(*user_col)), safe=False)

def is_user_exist(request, username):
	if User.objects.filter(email=username).exists():
		username = User.objects.get(email=username).username
	if User.objects.filter(username=username).exists():
		return JsonResponse(True, safe=False)
	else:
		return JsonResponse(False, safe=False)

def is_contractor_exist(request, company_name, id_card):
	if Contractor.objects.filter(contractor_company_name=company_name, contractor_id_card_number=id_card).exists():
		return JsonResponse(True, safe=False)
	else:
		return JsonResponse(False, safe=False)

def is_id_card_exist(request, id_card):
	is_exist = False
	if Visitor.objects.filter(visitor_id_card_number=id_card).exists():
		is_exist = True
	elif Employee.objects.filter(employee_id_card_number=id_card).exists():
		is_exist = True
	elif Contractor.objects.filter(contractor_id_card_number=id_card).exists():
		is_exist = True

	if is_exist:
		return JsonResponse(True, safe=False)
	else:
		return JsonResponse(False, safe=False)

def get_current_time_as_setting_timezone():
	setting_timezone = pytz.timezone(settings.TIME_ZONE)
	return datetime.datetime.now(setting_timezone)

def add_days_to_datetime(datetime_obj,add_days):
	return datetime_obj + datetime.timedelta(days=add_days)

def get_history_questions_list(request, history_id):
	history_col = ["user_course_question__question_title", "user_course_answer", "user_course_actual_answer", "user_course_question_weightage", "user_course_question_obtained_weightage"]
	return JsonResponse(list(UserCourseQuestionAnswer.objects.all().filter(user_course=UserCourse.objects.get(pk=history_id)).values(*history_col)), safe=False)

def update_completed_video(request, history_id, video_id):
	history_col = ["id", "user_course_all_videos", "user_course_completed_videos"]

	video_list = []
	history = UserCourse.objects.get(pk=history_id)
	if history.user_course_completed_videos:
		video_list = history.user_course_completed_videos.strip('][').split(',')

	video_list = list(map(int, video_list)) #convert all value to int
	video_list.append(int(video_id))
	
	history.user_course_completed_videos = str(video_list)
	history.save()
	return JsonResponse({"history":history.id if history.id else False})

def get_user_course_history(user_session, course_id):
	instance = User.objects.get(username=user_session['username'])

	if not UserCourse.objects.filter(user=instance.id, course=course_id).exists():
		history_id = create_user_course_history(instance.id,course_id)
		return {'history_id':history_id,'action':'Created'}
	elif UserCourse.objects.filter(user=instance.id, course=course_id).exists():
		last_attempted_course = UserCourse.objects.filter(user=instance.id, course=course_id).order_by('-user_course_attempt_date')[0]
		if last_attempted_course.user_course_status == 'Completed' and last_attempted_course.user_course_result == 'Fail':
			history_id = create_user_course_history(instance.id,course_id)
			return {'history_id':history_id,'action':'Created'}
		elif last_attempted_course.user_course_status == 'Completed' and last_attempted_course.user_course_result == 'Pass':
			if last_attempted_course.user_course_expiry_date.date() <  get_current_time_as_setting_timezone().date():
				history_id = create_user_course_history(instance.id,course_id)
				return {'history_id':history_id,'action':'Created'}
			elif last_attempted_course.user_course_expiry_date.date() >=  get_current_time_as_setting_timezone().date():
				return {'history_id':last_attempted_course.id,'action':'Completed'}
		elif last_attempted_course.user_course_status == 'Attempted' and last_attempted_course.user_course_result == 'Pending':
			return {'history_id':last_attempted_course.id,'action':'Attempted'}

def create_user_course_history(instance_id, course_id):
	user = User.objects.get(pk=instance_id)
	course = Course.objects.get(pk=course_id)

	user_course_history = UserCourse()
	user_course_history.user = user
	user_course_history.course = course

	video_list = []
	user_course_question_answer_list = []
	total_marks = 0

	added_question_list = []

	for video in course.course_video.all().filter(video_status='Active'):
		video_list.append(video.id)
		for question in video.video_question.all().filter(question_status='Active'):

			if question.id not in added_question_list:
				added_question_list.append(question.id)

				total_marks+= question.question_weightage

				user_course_question_answer = UserCourseQuestionAnswer()
				user_course_question_answer.user_course_question = question
				user_course_question_answer.user_course_answer = question.option_set.all().filter(option_is_answer=1)[0].option_title
				user_course_question_answer.user_course_question_weightage = question.question_weightage
				user_course_question_answer_list.append(user_course_question_answer)

	user_course_history.user_course_all_videos = str(video_list)
	user_course_history.user_course_total_marks = total_marks
	user_course_history.user_course_passig_percentage = course.course_passing_percentage
	user_course_history.user_course_validity = course.course_validity
	user_course_history.save()

	for user_course_question_answer in user_course_question_answer_list:
		user_course_question_answer.user_course = user_course_history
		user_course_question_answer.save()

	return user_course_history.id

def send_certificate_mail(request):
	user_session = get_user_session(request)
	if user_session:
		if request.method == 'POST':
			if not os.path.exists(settings.FILE_ROOT+'/'+'certificate_'+str(request.POST.get('history_id', False))+'.pdf'):
				
				#load basic html
				html_string =""
				with open(settings.FILE_ROOT+'/'+"base_certificate_html.html", 'r') as f:
					html_string = f.read()

				# set dynamic values to html
				html_string = html_string.format(username=user_session['first_name'] + ' ' + user_session['last_name'], coursename= request.POST.get('course_name', False), percentage=request.POST.get('percentage', False), completeddate=request.POST.get('completeddate', False), expirydate=request.POST.get('expirydate', False) if int(request.POST.get('course_validity', False)) > 0 else '--------')

				# write html file with dynamic values
				Html_file= open(settings.FILE_ROOT+'/'+"certificate_with_dynamic_values.html","w")
				Html_file.write(html_string)
				Html_file.close()

				# convert last updated html to pdf i.e certificate
				import pdfkit

				pdfkit.from_file(settings.FILE_ROOT+'/'+"certificate_with_dynamic_values.html", settings.FILE_ROOT+'/'+'certificate_'+str(request.POST.get('history_id', False))+'.pdf')

				# path_wkhtmltopdf = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
				# config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)
				# pdfkit.from_file(settings.FILE_ROOT+'/'+"certificate_with_dynamic_values.html", settings.FILE_ROOT+'/'+'certificate_'+str(request.POST.get('history_id', False))+'.pdf', configuration=config)

			# email body or message that will go into the mail of certificate
			email_body = """\
		    <html>
		      <head><title>User Registration Information</title></head>
		      <body>
		        Hello %s, get your certificate for the completion of course <b> %s </b>.
		      </body>
		    </html>
		    """% (user_session['first_name'], request.POST.get('course_name', False))

		    # send mail to user
			email = EmailMessage('Electrogas Certificate!', email_body, settings.DEFAULT_FROM_EMAIL, to=[user_session['email']])
			email.content_subtype = "html"
			email.attach_file(settings.FILE_ROOT+'/'+'certificate_'+str(request.POST.get('history_id', False))+'.pdf')
			email.send(fail_silently=True)

			messages.success(request, 'Email has been sent !')
			return HttpResponseRedirect(reverse('induction:certificate', kwargs={'history_id':request.POST.get('history_id', False)}))
		else:
			return HttpResponseRedirect(reverse('induction:login'))
	else:
		return HttpResponseRedirect(reverse('induction:login'))