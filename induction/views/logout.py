from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from induction.views.login import get_user_session
from induction.views.course import get_course_session
from induction.views.question import get_question_session

def logout(request):
	if request.method == 'POST':
		user_session = get_user_session(request)
		course_session = get_course_session(request)
		question_session = get_question_session(request)

		if user_session:
			request.session['user'] = None
			del request.session['user']

		if course_session:
			request.session['course'] = None
			del request.session['course']

		if question_session:
			request.session['question'] = None
			del request.session['question']

	return HttpResponseRedirect(reverse('induction:index'))