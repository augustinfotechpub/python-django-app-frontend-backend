from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse

from django.contrib.auth.models import User
from induction.models.contractor import Contractor
from induction.models.employee import Employee
from induction.models.visitor import Visitor
from induction.models.area import Area

from django.conf import settings
import json
import datetime, pytz
import re
import os

# regExs
regexEmail = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
regexPhone = '^(?!-.*$)([+]?)(?!-.*$)([0-9-]){8,14}?$'
image_extension_list = ['.png','.jpg', '.jpeg','.gif','.bmp']

# Create your views here.
def register(request):
	if request.method == 'POST':
		msg=''

		# If webcam image exist then assign o file object
		f = request.POST.get('webcamImg', None)
		if f:
			request.FILES['user_image'] = to_file(f)

		if request.POST['user_type'] == 'Contractor':
			try:
				msg=validate_register_form(request, 'Contractor')
				if len(msg):
					return render(request, 'induction/register.html', {'msg':msg})

				objContractor = Contractor()
				objContractor.username=request.POST['email']
				objContractor.first_name=request.POST['first_name']
				objContractor.last_name=request.POST['last_name']
				objContractor.contractor_phone_number=request.POST['phone']
				objContractor.email=request.POST['email']
				objContractor.contractor_company_name=request.POST.get('contractor_name', False)
				objContractor.contractor_id_card_number=request.POST.get('id_card', False) if request.POST.get('id_card', False) else None
				objContractor.contractor_photo=request.FILES.get('user_image', False)
				objContractor.is_active=False
				objContractor.is_staff=True
				objContractor.set_password(request.POST['password'])
				objContractor.save()

				msg = 'You have successfully registered as '+ str(request.POST['user_type']) + '. Your account will be activated soon by the admin'
			except:
				msg = 'Something went wrong, check if you provide a valid details or not'
				return render(request, 'induction/register.html', {'msg':msg})

		elif request.POST['user_type'] == 'Employee':
			try:
				msg=validate_register_form(request, 'Employee')
				if len(msg):
					return render(request, 'induction/register.html', {'msg':msg})

				objEmployee = Employee()
				objEmployee.username = request.POST['email']
				objEmployee.first_name=request.POST['first_name']
				objEmployee.last_name=request.POST['last_name']
				objEmployee.employee_phone_number=request.POST['phone']
				objEmployee.email=request.POST['email']
				objEmployee.employee_contractor= Contractor.objects.get(username=request.POST['contractor_name'])
				objEmployee.employee_id_card_number=request.POST.get('id_card', False) if request.POST.get('id_card', False) else None
				objEmployee.employee_photo=request.FILES.get('user_image', False)
				objEmployee.employee_area = Area.objects.get(pk=request.POST['area_name'])
				objEmployee.is_active=True
				objEmployee.set_password(request.POST['password'])
				objEmployee.save()
				msg = 'You have successfully registered as '+ str(request.POST['user_type'])
			except:
				msg = 'Something went wrong, check if you provide a valid details or not'
				return render(request, 'induction/register.html', {'msg':msg})

		elif request.POST['user_type'] == 'Visitor':
			try:
				msg=validate_register_form(request, 'Visitor')
				if len(msg):
					return render(request, 'induction/register.html', {'msg':msg})

				objVisitor = Visitor()
				objVisitor.username = request.POST['email']
				objVisitor.first_name=request.POST['first_name']
				objVisitor.last_name=request.POST['last_name']
				objVisitor.visitor_phone_number=request.POST['phone']
				objVisitor.email=request.POST['email']
				objVisitor.visitor_id_card_number=request.POST.get('id_card', False) if request.POST.get('id_card', False) else None
				objVisitor.visitor_photo=request.FILES.get('user_image', False)
				objVisitor.visitor_employee=request.POST['employee_name']
				objVisitor.visitor_area_to_visit=Area.objects.get(pk=request.POST['area_name'])
				datetime_of_visit_naive = datetime.datetime.strptime(request.POST['visit_datetime'], '%m/%d/%Y %H:%M:%S')
				datetime_of_visit = pytz.timezone(settings.TIME_ZONE).localize(datetime_of_visit_naive)
				
				objVisitor.visitor_datetime_of_visit= datetime_of_visit
				objVisitor.visitor_purpose_of_visit=request.POST['purpose_of_visit']
				objVisitor.is_active=True
				objVisitor.set_password(request.POST['password'])
				objVisitor.save()

				msg = 'You have successfully registered as '+ str(request.POST['user_type'])
			except:
				msg = 'Something went wrong, check if you provide a valid details or not'
				return render(request, 'induction/register.html', {'msg':msg})

		return render(request, 'induction/register.html', {'msg':msg, 'success':True})
	else:
		return render(request, 'induction/register.html')

def validate_register_form(request, user_type):
	msg = ''
	msg_count = 0
	email = request.POST.get('email', False)
	first_name = request.POST.get('first_name', False)
	last_name = request.POST.get('last_name', False)
	phone = request.POST.get('phone', False)
	id_card = request.POST.get('id_card', False)
	user_image = request.FILES.get('user_image', False)
	password = request.POST.get('password', False)
	confirm_password = request.POST.get('confirm_password', False)

	visit_datetime = request.POST.get('visit_datetime', False)
	employee_name = request.POST.get('employee_name', False)
	purpose_of_visit = request.POST.get('purpose_of_visit', False)

	area_name = request.POST.get('area_name', False)
	# company_name = request.POST.get('company_name', False)
	contractor_name = request.POST.get('contractor_name', False)
	accept = request.POST.get('accept', False)

	if User.objects.filter(username=email).exists() or User.objects.filter(email=email).exists():
		msg_count+=1
		msg += '<li>Email already registered</li>'
	if not re.search(regexEmail,email):
		msg_count+=1
		msg += '<li>Please provide a valid email</li>'
	if not first_name or not last_name:
		msg_count+=1
		msg += '<li>Both first name and last name are required</li>'
	if len(first_name) > 30 or len(last_name) > 30:
		msg_count+=1
		msg += '<li>Maximum length of first name and last name should be 30</li>'
	if phone:
		if not re.search(regexPhone,phone):
			msg_count+=1
			msg += '<li>Please provide a valid phone number</li>'
	if user_type != 'Contractor':
		if not id_card or len(id_card) > 20:
			msg_count+=1
			msg += '<li>ID is required, maximum length should be 20</li>'
	if not len(password) >= 8:
		msg_count+=1
		msg += '<li>Your password must be at least 8 characters long</li>'
	if not password == confirm_password:
		msg_count+=1
		msg += '<li>Password and confirm password should be matched</li>'
	if user_image:
		if get_file_extension(user_image) not in image_extension_list:
			msg_count+=1
			msg += '<li>Only images are allowed</li>'

		if len(user_image.read())  > 209716:
			msg_count+=1
			msg += '<li>Max file size is 200 kb</li>'

	if user_type == 'Visitor':
		is_valid_date = True
		try:
			date_time = datetime.datetime.strptime(visit_datetime, '%m/%d/%Y %H:%M:%S')
		except:
			msg_count+=1
			msg+= '<li>Provide a valid datetime in format "mm/dd/YYY H:M:S"</li>'

		if not employee_name:
			msg_count+=1
			msg += '<li>Employee name is required</li>'

		if not purpose_of_visit or len(purpose_of_visit) > 200:
			msg_count+=1
			msg += '<li>Purpose of visit is required, maximum length should be 200</li>'

		if not area_name:
			msg_count+=1
			msg += '<li>Area is required</li>'
	
	if user_type == 'Contractor':
		if not contractor_name or not Contractor.objects.filter(username=contractor_name).exists():
			msg_count+=1
			msg += '<li>Valid contractor name is required</li>'
		if contractor_name and id_card:
			if Contractor.objects.filter(contractor_company_name=contractor_name, contractor_id_card_number=id_card).exists():
				msg_count+=1
				msg += '<li>Id card number with given contractor is already registered</li>'

	if user_type == 'Employee':
		if not contractor_name or not Contractor.objects.filter(username=contractor_name).exists():
			msg_count+=1
			msg += '<li>Valid contractor name is required</li>'

		if not area_name:
			msg_count+=1
			msg += '<li>Area is required</li>'

	if not accept:
		msg_count+=1
		msg += '<li>You should agree to the terms and conditions</li>'

	if msg_count > 1:
		msg = '<ul>'+ msg +'</ul>'

	return msg

def get_file_extension(filename):
	if filename:
		file_name, file_extension = os.path.splitext(str(filename))
		return file_extension.lower()
	else:
		return False

def to_file(file_from_POST):
	"""base64 encoded file to Django InMemoryUploadedFile that can be placed into request.FILES."""
	# 'data:image/png;base64,<base64 encoded string>'
	import base64
	import io
	import sys
	from django.core.files.uploadedfile import InMemoryUploadedFile
	from django.core.exceptions import SuspiciousOperation
	try:
		idx = file_from_POST[:50].find(',')  # comma should be pretty early on

		if not idx or not file_from_POST.startswith('data:image/'):
			raise Exception()

		base64file = file_from_POST[idx+1:]
		attributes = file_from_POST[:idx]
		content_type = attributes[len('data:'):attributes.find(';')]
	except Exception as e:
		raise SuspiciousOperation("Invalid picture")

	f = io.BytesIO(base64.b64decode(base64file))
	image = InMemoryUploadedFile(f,
       field_name='user_image',
       name='picture.png',  # use UUIDv4 or something
       content_type=content_type,
       size=sys.getsizeof(f),
       charset=None)
	return image