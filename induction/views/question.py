from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.shortcuts import get_list_or_404, get_object_or_404
from induction.views.api import get_current_time_as_setting_timezone, add_days_to_datetime
from induction.views.login import get_user_session
from induction.views.course import get_course_session
from induction.models.video import Video
from induction.models.user_course import UserCourse
from django.core.paginator import Paginator

# Create your views here.
def go_to_question(request):
	user_session = get_user_session(request)
	course_session = get_course_session(request)
	question_session = get_question_session(request)

	if user_session:
		if course_session:
			if question_session:
				if question_session['allowed']:
					return HttpResponseRedirect(reverse('induction:question'))
			else:
				if request.method == 'POST':
					request.session['question'] = {"allowed":True}
					return HttpResponseRedirect(reverse('induction:question'))

			return HttpResponseRedirect(reverse('induction:course_detail', kwargs={'course_id':course_session['course_id']}))
		else:
			return HttpResponseRedirect(reverse('induction:course'))
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def question(request):
	user_session = get_user_session(request)
	course_session = get_course_session(request)
	question_session = get_question_session(request)

	if user_session:
		if course_session:
			if question_session:
				if question_session['allowed']:

					if request.method == 'GET':

						question_list_history = get_object_or_404(UserCourse, pk=course_session['history_id']).usercoursequestionanswer_set.all()
						return render(request, 'induction/question_list.html', {'user_session':user_session, 'course_session': course_session, 'question_session':question_session, 'question_list_history': question_list_history})

					elif request.method == 'POST':

						user_course_history = get_object_or_404(UserCourse, pk=course_session['history_id'])
						question_list_history = user_course_history.usercoursequestionanswer_set.all()
						obtained_marks = 0

						for question in question_list_history:
							question.user_course_actual_answer = request.POST.get(str(question.id), False)
							if str(question.user_course_actual_answer) == str(question.user_course_answer):
								question.user_course_question_obtained_weightage = question.user_course_question_weightage
								obtained_marks+=question.user_course_question_weightage
							else:
								question.user_course_question_obtained_weightage = 0

							question.save()

						user_course_history.user_course_obtained_marks = obtained_marks

						obtained_percentage = (float(obtained_marks)*100)/float(user_course_history.user_course_total_marks)
						user_course_history.user_course_obtained_percentage = obtained_percentage

						current_datetime = get_current_time_as_setting_timezone()

						if obtained_percentage >= user_course_history.user_course_passig_percentage:
							user_course_history.user_course_result = 'Pass'
							user_course_history.user_course_expiry_date = add_days_to_datetime(current_datetime,user_course_history.user_course_validity)
						else:
							user_course_history.user_course_result = 'Fail'

						user_course_history.user_course_complete_date = current_datetime

						user_course_history.user_course_status = 'Completed'
						user_course_history.save()

						#destroy course and question session
						request.session['course'] = None
						del request.session['course']
						request.session['question'] = None
						del request.session['question']

						return HttpResponseRedirect(reverse('induction:result', kwargs={'history_id':user_course_history.id}))

			return HttpResponseRedirect(reverse('induction:course_detail', kwargs={'course_id':course_session['course_id']}))
		else:
			return HttpResponseRedirect(reverse('induction:course'))
	else:
		return HttpResponseRedirect(reverse('induction:login'))

def get_question_session(request):
	question_session = None
	if "question" in request.session:
		if request.session['question']:
			question_session = request.session['question']

	return question_session
