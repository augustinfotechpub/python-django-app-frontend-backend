from django.db import models
from induction.models.custom_fields import PositiveTinyIntegerField, PositiveFloatField

# Create your models here.
class Question(models.Model):
	INACTIVE='Inactive'
	ACTIVE='Active'
	STATUS_CHOICES = (
    	(INACTIVE, 'Inactive'),
    	(ACTIVE, 'Active')
    )

	question_title = models.CharField('Question Title', max_length=1000)
	question_weightage = PositiveFloatField('Weight')
	question_status = models.CharField('Status', max_length=8, choices=STATUS_CHOICES, default=ACTIVE)
	question_created_date = models.DateTimeField(auto_now_add=True)
	question_updated_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.question_title)

	class Meta:
		app_label = 'induction'