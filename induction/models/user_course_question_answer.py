from django.db import models
from django.contrib.auth.models import User
from induction.models.course import Course
from induction.models.custom_fields import PositiveTinyIntegerField, PositiveSmallIntegerField, PositiveFloatField

from induction.models.user_course import UserCourse
from induction.models.question import Question

class UserCourseQuestionAnswer(models.Model):
	user_course = models.ForeignKey(UserCourse, on_delete=models.CASCADE, verbose_name = 'User course')
	user_course_question = models.ForeignKey(Question, on_delete=models.PROTECT, verbose_name = 'Course question')
	user_course_answer = models.CharField(max_length=100)
	user_course_actual_answer = models.CharField(max_length=100, blank=True, null=True)
	user_course_question_weightage = PositiveFloatField('Qustion weightage')
	user_course_question_obtained_weightage = PositiveFloatField('Obtained weightage', blank=True, null=True)

	def __str__(self):
		return str(self.id)

	class Meta:
		app_label = 'induction'