from django.db import models

# Create your models here.
class SiteSetting(models.Model):
	site_name = models.CharField('Site Name', max_length=15)
	welcome_note_title = models.CharField('Welcome Note title', max_length=50)
	welcome_note = models.CharField('Welcome Note', max_length=2000)
	course_list_note_title = models.CharField('Course list Note title', max_length=50)
	course_list_note = models.CharField('Note about Course list', max_length=2000)
	admin_contact = models.CharField('Admin Contact', max_length=15)
	admin_email = models.EmailField('Admin Email', max_length=60)

	class Meta:
		app_label = 'induction'