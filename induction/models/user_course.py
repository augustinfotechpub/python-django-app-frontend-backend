from django.db import models
from django.contrib.auth.models import User
from induction.models.course import Course
from induction.models.custom_fields import PositiveTinyIntegerField, PositiveSmallIntegerField, PositiveFloatField

class UserCourse(models.Model):
	PENDING='Pending'
	ATTEMPTED='Attempted'
	COMPLETED='Completed'

	COURSE_STATUS_CHOICES = ((ATTEMPTED, 'Attempted'),(COMPLETED, 'Completed'))
	
	PASS='Pass'
	FAIL='Fail'

	COURSE_RESULT_CHOICES = ((PENDING, 'Pending'),(PASS, 'Pass'),(FAIL, 'Fail'))

	user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name = 'User')
	course = models.ForeignKey(Course, on_delete=models.PROTECT, verbose_name = 'Course')
	user_course_status = models.CharField('Course Status', max_length=9, choices=COURSE_STATUS_CHOICES, default=ATTEMPTED)
	user_course_result = models.CharField('Course Result', max_length=7, choices=COURSE_RESULT_CHOICES, default=PENDING)
	user_course_all_videos = models.CharField('All Videos', max_length=100)
	user_course_completed_videos = models.CharField('Completed Videos', max_length=100, blank=True, null=True)
	user_course_total_marks = models.DecimalField('Total Marks', max_digits=7, decimal_places=2)
	user_course_obtained_marks = models.DecimalField('Obtained Marks', max_digits=7, decimal_places=2, blank=True, null=True)
	user_course_passig_percentage = PositiveFloatField('Passing Percentage [%]')
	user_course_obtained_percentage = PositiveFloatField('Obtained Percentage [%]', blank=True, null=True)
	user_course_validity = PositiveSmallIntegerField('Validity [days]', help_text="0 for no course validity.")
	user_course_attempt_date = models.DateTimeField('Attempted on', auto_now_add=True)
	user_course_complete_date = models.DateTimeField('Completed on', blank=True, null=True)
	user_course_expiry_date = models.DateTimeField('Expiry date', blank=True, null=True)

	# def __str__(self):
	# 	return str(self.user + '-' +self.course)

	class Meta:
		app_label = 'induction'