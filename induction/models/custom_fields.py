from django.db import models

# Create custom type field.
class PositiveTinyIntegerField(models.PositiveSmallIntegerField):
    def db_type(self, connection):
        if connection.settings_dict['ENGINE'] == 'django.db.backends.mysql':
            return "tinyint unsigned"
        else:
            return super(PositiveTinyIntegerField, self).db_type(connection)

    def formfield(self, **kwargs):
        defaults = {'min_value': 1, 'max_value':100}
        defaults.update(kwargs)
        return super(PositiveTinyIntegerField, self).formfield(**defaults)

class PositiveSmallIntegerField(models.SmallIntegerField):
    def db_type(self, connection):
        if connection.settings_dict['ENGINE'] == 'django.db.backends.mysql':
            return "smallint unsigned"
        else:
            return super(PositiveSmallIntegerField, self).db_type(connection)

    def formfield(self, **kwargs):
        defaults = {'min_value': 0, 'max_value':9999}
        defaults.update(kwargs)
        return super(PositiveSmallIntegerField, self).formfield(**defaults)

class PositiveFloatField(models.FloatField):
    def db_type(self, connection):
        if connection.settings_dict['ENGINE'] == 'django.db.backends.mysql':
            return "float unsigned"
        else:
            return super(PositiveFloatField, self).db_type(connection)

    def formfield(self, **kwargs):
        defaults = {'min_value': 0, 'max_value':100}
        defaults.update(kwargs)
        return super(PositiveFloatField, self).formfield(**defaults)