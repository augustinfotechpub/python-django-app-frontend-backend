from django.db import models
from induction.models.question import Question

# Create your models here.
class Option(models.Model):
	option_question = models.ForeignKey(Question, on_delete=models.CASCADE)
	option_title = models.CharField(max_length=1000)
	option_is_answer = models.BooleanField('Is Answer', default=False)

	def __str__(self):
		return str(self.option_title)

	class Meta:
		app_label = 'induction'