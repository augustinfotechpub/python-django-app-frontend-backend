from django.db import models
from django.contrib.auth.models import User
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from induction.models.course import Course

# Create your models here.
@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)

class Contractor(User):
	contractor_course = models.ManyToManyField(Course, blank=True)
	contractor_company_name = models.CharField('Contractor', max_length=150, blank=True, null=True)
	contractor_phone_number = models.CharField('Phone Number', max_length=15, blank=True, null=True)
	contractor_photo = models.ImageField('Photo', upload_to=PathAndRename(''), blank=True)
	contractor_id_card_number = models.CharField('ID Card No', max_length=20, unique=True, blank=True, null=True)
	contractor_is_main = models.BooleanField('Is Main Contractor?',default=False)
	contractor_created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
	contractor_updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)

	def __str__(self):
		return str((self.contractor_company_name) + (('-' +self.contractor_id_card_number) if self.contractor_id_card_number else ''))

	class Meta:
		app_label = 'induction'
		unique_together = ('contractor_company_name', 'contractor_id_card_number',)