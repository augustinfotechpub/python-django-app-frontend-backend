from django.db import models
from induction.models.custom_fields import PositiveTinyIntegerField, PositiveSmallIntegerField, PositiveFloatField
from induction.models.video import Video

# Create your models here.
class Course(models.Model):
	INACTIVE='Inactive'
	ACTIVE='Active'
	STATUS_CHOICES = (
    	(INACTIVE, 'Inactive'),
    	(ACTIVE, 'Active')
    )

	course_title = models.CharField('Course Title', max_length=255)
	course_description = models.CharField(max_length=2000)
	course_validity = PositiveSmallIntegerField('Validity [days]', help_text="Enter 0 for no course validity.")
	course_passing_percentage = PositiveFloatField('Passing Percentage [%]')

	course_video = models.ManyToManyField(Video)

	course_auto_assigned = models.BooleanField(default=False)
	course_status = models.CharField('Status', max_length=8, choices=STATUS_CHOICES, default=ACTIVE)
	course_created_date = models.DateTimeField(auto_now_add=True)
	course_updated_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.course_title)

	class Meta:
		app_label = 'induction'