from django.db import models           
from induction.models.question import Question

# Create your models here.		
class Video(models.Model):
	YOUTUBE = 'youtube'
	VIDEO_TYPE_CHOICES = (
        (YOUTUBE, 'Youtube'),
    )

	INACTIVE='Inactive'
	ACTIVE='Active'
	STATUS_CHOICES = (
    	(INACTIVE, 'Inactive'),
    	(ACTIVE, 'Active')
    )
	
	video_title = models.CharField('Video Title', max_length=255)
	video_url = models.CharField('Youtube URL', max_length=255, help_text='example: https://www.youtube.com/watch?v=2JjSMA2GJ9o')
	video_type = models.CharField('Type', max_length=10, choices=VIDEO_TYPE_CHOICES, default=YOUTUBE)

	video_question = models.ManyToManyField(Question)

	video_status = models.CharField('Status', max_length=8, choices=STATUS_CHOICES, default=ACTIVE)
	video_created_date = models.DateTimeField(auto_now_add=True)
	video_updated_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.video_title)

	class Meta:
		app_label = 'induction'
		ordering = ['-id']