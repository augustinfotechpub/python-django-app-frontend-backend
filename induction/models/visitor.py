from django.db import models
from django.contrib.auth.models import User
import os
from datetime import datetime
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from induction.models.course import Course
from induction.models.area import Area

# Create your models here.
@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)

class Visitor(User):
	visitor_course = models.ManyToManyField(Course, blank=True)
	visitor_phone_number = models.CharField('Phone Number', max_length=15, blank=True, null=True)
	visitor_photo = models.ImageField('Photo', upload_to=PathAndRename(''), blank=True)
	visitor_id_card_number = models.CharField('ID Card No', max_length=20, blank=True, null=True, unique=True)

	visitor_datetime_of_visit = models.DateTimeField('Visiting date', default=datetime.now, blank=True, null=True)
	visitor_purpose_of_visit = models.CharField(max_length=200, blank=True, null=True)
	visitor_area_to_visit = models.ForeignKey(Area, on_delete=models.CASCADE, verbose_name = 'Area to visit')
	visitor_employee = models.CharField('Meet up with', max_length=30, blank=True, null=True)

	visitor_created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
	visitor_updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)

	def __str__(self):
		return str(self.username)

	class Meta:
		app_label = 'induction'