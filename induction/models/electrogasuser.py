from django.db import models
from django.contrib.auth.models import User
import os
from datetime import datetime
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from induction.models.course import Course
from django.contrib.auth.models import PermissionsMixin

# Create your models here.
@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)

class AdminUser(User, PermissionsMixin):
    user_phone_number = models.CharField('Phone Number', max_length=15, blank=True, null=True)
    user_course = models.ManyToManyField(Course, blank=True)
    user_photo = models.ImageField('Photo', upload_to=PathAndRename(''), blank=True)
    user_id_card_number = models.CharField('ID Card No', max_length=20, blank=True, null=True)
    def __str__(self):
        return str(self.username)
    class Meta:
        app_label = 'induction'