from django.db import models           

# Create your models here.		
class Area(models.Model):
	area_name = models.CharField('Area Name', max_length=255)

	def __str__(self):
		return str(self.area_name)

	class Meta:
		app_label = 'induction'
		ordering = ['-id']