from django.db import models
from django.contrib.auth.models import User
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from induction.models.contractor import Contractor
from induction.models.course import Course
from induction.models.area import Area

# Create your models here.
@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)

class Employee(User):
	#employee_course = models.ManyToManyField(Course, blank=True, null=True)
	employee_contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE, verbose_name = 'Contractor')
	employee_phone_number = models.CharField('Phone Number', max_length=15, blank=True, null=True)
	employee_photo = models.ImageField('Photo', upload_to=PathAndRename(''), blank=True)
	employee_id_card_number = models.CharField('ID/Passport Number', max_length=20, blank=True, null=True, unique=True)
	employee_area = models.ForeignKey(Area, on_delete=models.CASCADE, verbose_name = 'Area')
	employee_created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
	employee_updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)

	def __str__(self):
		return str(self.username)

	class Meta:
		app_label = 'induction'