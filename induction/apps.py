from django.apps import AppConfig

class InductionConfig(AppConfig):
    name = 'induction'

    def ready(self):
        import induction.signals.handlers