"""
WSGI config for electrogas project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""
import sys
import os

from django.core.wsgi import get_wsgi_application
sys.path.append('/home/staging/public_html')
sys.path.append('/home/staging/public_html/induction')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'electrogas.settings')

application = get_wsgi_application()
