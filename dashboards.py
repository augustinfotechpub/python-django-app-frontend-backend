from controlcenter import Dashboard, widgets
from django.contrib.auth.models import User
from induction.models.electrogasuser import AdminUser
from induction.models.contractor import Contractor
from induction.models.super_contractor import SuperContractor
from induction.models.employee import Employee
from induction.models.visitor import Visitor
from induction.models.course import Course
from induction.models.video import Video
from induction.models.question import Question
from induction.models.user_course import UserCourse
from django.contrib.admin.models import LogEntry
from datetime import datetime
import datetime as datetimeobj
import pytz
from django.conf import settings
from django.utils.safestring import mark_safe
import os
from django.db import connection

class AdminUserWidget(widgets.ItemList):
    def name(self,obj):
        return obj.get_full_name()
    def profile(self, obj):
        img_url = obj.user_photo.url if obj.user_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
        return mark_safe('<img src="{url}" width="35" height="35" />'.format(url = img_url))
    model = AdminUser
    queryset = AdminUser.objects.all().filter(is_active=True).order_by('-id')
    list_display = ('profile', 'username', 'email', 'name')
    list_display_links = ['profile', 'username']
    title = "Admin Users"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class ContractorWidget(widgets.ItemList):
    def name(self,obj):
        return obj.get_full_name()
    def profile(self, obj):
        img_url = obj.contractor_photo.url if obj.contractor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
        return mark_safe('<img src="{url}" width="35" height="35" />'.format(url = img_url))
    model = Contractor
    queryset = Contractor.objects.all().filter(is_active=True).order_by('-id')
    list_display = ('profile', 'username', 'contractor_company_name', 'contractor_id_card_number', 'contractor_phone_number')
    list_display_links = ['profile', 'username']
    title = "Contractors"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class EmployeeWidget(widgets.ItemList):
    def name(self,obj):
        return obj.get_full_name()
    def profile(self, obj):
        img_url = obj.employee_photo.url if obj.employee_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
        return mark_safe('<img src="{url}" width="35" height="35" />'.format(url = img_url))
    model = Employee
    queryset = Employee.objects.all().filter(is_active=True).order_by('-id')
    list_display = ('profile', 'name', 'employee_id_card_number', 'employee_phone_number', 'employee_contractor')
    list_display_links = ['profile', 'name']
    title = "Employees"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class VisitorWidget(widgets.ItemList):
    def name(self,obj):
        return obj.get_full_name()
    def profile(self, obj):
        img_url = obj.visitor_photo.url if obj.visitor_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
        return mark_safe('<img src="{url}" width="35" height="35" />'.format(url = img_url))
    model = Visitor
    queryset = Visitor.objects.all().filter(is_active=True).order_by('-id')
    list_display = ('profile', 'name', 'visitor_id_card_number', 'visitor_phone_number', 'visitor_datetime_of_visit', 'visitor_employee')
    list_display_links = ['profile', 'name']
    title = "Visitors"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class CourseWidget(widgets.ItemList):
    model = Course
    queryset = Course.objects.all().filter(course_status='Active').order_by('-id')
    list_display = ('course_title', 'course_validity', 'course_passing_percentage')
    list_display_links = ['course_title']
    title = "Courses"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class VideoWidget(widgets.ItemList):
    model = Video
    queryset = Video.objects.all().filter(video_status='Active').order_by('-id')
    list_display = ('video_title', 'video_url')
    list_display_links = ['video_title']
    title = "Videos"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class QuestionWidget(widgets.ItemList):
    model = Question
    queryset = Question.objects.all().filter(question_status='Active').order_by('-id')
    list_display = ('question_title', 'question_weightage')
    list_display_links = ['question_title']
    title = "Questions"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class UserWidget(widgets.ItemList):
    def user(self, obj):
        content_type = ''
        if obj.groups.all().filter(name='Contractor').exists():
            content_type = 'contractor'
        elif obj.groups.all().filter(name='Employee').exists():
            content_type = 'employee'
        elif obj.groups.all().filter(name='Visitor').exists():
            content_type = 'visitor'
        else:
            content_type = 'adminuser'

        admin_url = '/overseer/induction/{content_type}/{id}/change/'.format(content_type= content_type, id=obj.id)
        return mark_safe('<a href="{admin_url}">{username}</a>'.format(admin_url = admin_url, username = obj.username ))
    def group(self, obj):
        group = ''
        if obj.groups.all().filter(name='Contractor').exists():
            group = 'Contractor'
        elif obj.groups.all().filter(name='Employee').exists():
            group = 'Employee'
        elif obj.groups.all().filter(name='Visitor').exists():
            group = 'Visitor'
        else:
            group = 'Admin User'
        return group
    def date_joined(self, obj):
        utc_date = obj.date_joined
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%d, %b %Y')
    def time(self, obj):
        utc_date = obj.date_joined
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%I:%M %p')
    model = User
    queryset = User.objects.all().filter().exclude(id=1).order_by('-date_joined')
    list_display = ('user', 'group', 'date_joined', 'time')
    list_display_links = ['user']
    title = "Recently Registered Users"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class HistoryWidget(widgets.ItemList):
    def course(self, obj):
        admin_url = '/overseer/induction/course/{id}/change/'.format(id=obj.course.id)
        return mark_safe('<a href="{admin_url}">{course}</a>'.format(admin_url = admin_url, course = obj.course ))
    def user(self, obj):
        content_type = ''
        if obj.user.groups.all().filter(name='Contractor').exists():
            content_type = 'contractor'
        elif obj.user.groups.all().filter(name='Employee').exists():
            content_type = 'employee'
        elif obj.user.groups.all().filter(name='Visitor').exists():
            content_type = 'visitor'
        else:
            content_type = 'adminuser'

        admin_url = '/overseer/induction/{content_type}/{id}/change/'.format(content_type= content_type, id=obj.user.id)
        return mark_safe('<a href="{admin_url}">{username}</a>'.format(admin_url = admin_url, username = obj.user.username ))
    def user_course_complete_date(self, obj):
        utc_date = obj.user_course_complete_date
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%d, %b %Y')
    def time(self, obj):
        utc_date = obj.user_course_complete_date
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%I:%M %p')
    model = UserCourse
    queryset = UserCourse.objects.all().filter(user_course_status='Completed', user_course_result='Pass').order_by('-user_course_complete_date')
    list_display = ('course', 'user', 'user_course_complete_date','time')
    # list_display_links = ['username']
    title = "Recently Completed Courses"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class LogEntryWidget(widgets.ItemList):
    def action(self, obj):
        object_name= obj.object_repr
        admin_url = obj.get_admin_url()
        action_class = 'changelink' if obj.action_flag == 2 else 'addlink' if obj.action_flag == 1 else 'deletelink'
        if action_class != 'deletelink':
            return mark_safe('<a href="{admin_url}" class="{action_class}">{object_name}</a><br><span class="mini quiet" style="padding-left: 17px;">{content_type}</span>'.format(admin_url = admin_url, object_name = object_name, action_class = action_class, content_type=str(obj.content_type).upper() ))
        else:
            return mark_safe('<span class="{action_class}">{object_name}</span><br><span class="mini quiet" style="padding-left: 17px;">{content_type}</span>'.format(object_name = object_name, action_class = action_class, content_type=str(obj.content_type).upper() ))
    def user(self, obj):
        content_type = ''
        if obj.user.groups.all().filter(name='Contractor').exists():
            content_type = 'contractor'
        elif obj.user.groups.all().filter(name='Employee').exists():
            content_type = 'employee'
        elif obj.user.groups.all().filter(name='Visitor').exists():
            content_type = 'visitor'
        else:
            content_type = 'adminuser'

        admin_url = '/overseer/induction/{content_type}/{id}/change/'.format(content_type= content_type, id=obj.user.id)
        return mark_safe('<a href="{admin_url}">{username}</a>'.format(admin_url = admin_url, username = obj.user.username ))
    def date(self, obj):
        utc_date = obj.action_time
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%d, %b %Y')
    def time(self, obj):
        utc_date = obj.action_time
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%I:%M %p')

    model = LogEntry
    queryset = LogEntry.objects.all().exclude(user_id=1).order_by('-action_time')
    list_display = ('action', 'user', 'date', 'time')
    title = "Recent Actions by User"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE


class UserCourseStatusChart(widgets.SinglePieChart):
    limit_to = 10
    width = widgets.LARGE

    def series(self):
        user_query = '''SELECT user_id, date_joined FROM ((SELECT user_ptr_id as user_id, date_joined FROM induction_adminuser LEFT JOIN auth_user ON id = user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, date_joined FROM induction_contractor LEFT JOIN auth_user ON id = user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, date_joined FROM induction_visitor LEFT JOIN auth_user ON id = user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, date_joined FROM induction_employee LEFT JOIN auth_user ON id = user_ptr_id)) AS group_table'''
        filter_value = datetime.now().year
        filter_dict = {'1':'current_year', '2':'previous_year', '3':'current_month', '4':'previous_month'}
        
        if self.request.GET.get('f', False) and self.request.GET.get('f', False) in filter_dict:
            if filter_dict[self.request.GET.get('f', False)] == 'current_year':
                filter_value = int(datetime.now().year)
                user_query += ' WHERE YEAR(date_joined) = {0}'.format(filter_value)
            elif filter_dict[self.request.GET.get('f', False)] == 'previous_year':
                filter_value = int(datetime.now().year) - 1
                user_query += ' WHERE YEAR(date_joined) = {0}'.format(filter_value)
            elif filter_dict[self.request.GET.get('f', False)] == 'current_month':
                filter_value = int(datetime.now().month)
                user_query += ' WHERE MONTH(date_joined) = {0}'.format(filter_value)
            elif filter_dict[self.request.GET.get('f', False)] == 'previous_month':
                today = datetimeobj.date.today()
                first = today.replace(day=1)
                filter_value = (first - datetimeobj.timedelta(days=1)).month
                user_query += ' WHERE MONTH(date_joined) = {0}'.format(filter_value)
        else:
            user_query += ' WHERE YEAR(date_joined) = {0}'.format(filter_value)

        user_cursor = connection.cursor()
        user_cursor.execute(user_query)
        
        completed_user_list = []
        attempted_user_list = []
        not_attempted_user_list = []

        for i in user_cursor.fetchall():
            assigned_cursor = connection.cursor()
            assigned_cursor.execute('''SELECT count(user_id) as assigned_course FROM ((SELECT user_ptr_id as user_id, course_id FROM induction_adminuser LEFT JOIN induction_adminuser_user_course ON induction_adminuser_user_course.adminuser_id = induction_adminuser.user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, course_id FROM induction_contractor LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, course_id FROM induction_visitor LEFT JOIN induction_visitor_visitor_course ON induction_visitor_visitor_course.visitor_id = induction_visitor.user_ptr_id) UNION ALL (SELECT induction_employee.user_ptr_id as user_id, course_id FROM induction_employee LEFT JOIN induction_contractor ON induction_contractor.user_ptr_id = induction_employee.employee_contractor_id LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id)) as grouptable WHERE course_id is not null and user_id={0} GROUP BY user_id'''.format(i[0]))
            assigned_course = assigned_cursor.fetchone()
            assigned_cursor.close()

            assigned_course_list_cursor = connection.cursor()
            assigned_course_list_cursor.execute('''SELECT course_id FROM ((SELECT user_ptr_id as user_id, course_id FROM induction_adminuser LEFT JOIN induction_adminuser_user_course ON induction_adminuser_user_course.adminuser_id = induction_adminuser.user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, course_id FROM induction_contractor LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id) UNION ALL (SELECT user_ptr_id as user_id, course_id FROM induction_visitor LEFT JOIN induction_visitor_visitor_course ON induction_visitor_visitor_course.visitor_id = induction_visitor.user_ptr_id) UNION ALL (SELECT induction_employee.user_ptr_id as user_id, course_id FROM induction_employee LEFT JOIN induction_contractor ON induction_contractor.user_ptr_id = induction_employee.employee_contractor_id LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id)) as grouptable WHERE course_id is not null and user_id={0}'''.format(i[0]))

            course_list_str = ''
            count=0
            for c in assigned_course_list_cursor.fetchall():
                count+=1
                if count == 1:
                    course_list_str+= str(c[0])
                else:
                    course_list_str+= ',' + str(c[0])

            completed_cursor = connection.cursor()
            completed_course_query = '''SELECT count(id) as completed_course FROM `induction_usercourse` WHERE user_course_status = 'Completed' and user_course_result = 'Pass' and user_id = {}'''.format(i[0])
            if len(course_list_str) > 0:
                completed_course_query += ' AND course_id in ({})'.format(course_list_str)
                
            completed_cursor.execute(completed_course_query)
            completed_course = completed_cursor.fetchone()
            completed_cursor.close()

            attempted_cursor = connection.cursor()
            attempted_cursor_query = '''SELECT count(id) as attempted_course FROM `induction_usercourse` WHERE user_course_status = 'Attempted' and user_id = {}'''.format(i[0])
            if len(course_list_str) > 0:
                attempted_cursor_query += ' AND course_id in ({})'.format(course_list_str)

            attempted_cursor.execute(attempted_cursor_query)
            attempted_course = attempted_cursor.fetchone()
            attempted_cursor.close()

            if assigned_course:
                if int(assigned_course[0]) == int(completed_course[0]):
                    completed_user_list.append(i[0])
                    # print(str(assigned_course[0]) + ' | ' + str(completed_course[0]) + ' | ' + 'Completed')
                elif (int(assigned_course[0]) > int(completed_course[0])) and int(completed_course[0]) > 0:
                    attempted_user_list.append(i[0])
                    # print(str(assigned_course[0]) + ' | ' + str(completed_course[0]) + ' | ' + 'Attempted')
                else:
                    if int(attempted_course[0]) > 0:
                        attempted_user_list.append(i[0])
                    else:
                        not_attempted_user_list.append(i[0])
                    # print(str(assigned_course[0]) + ' | ' + str(completed_course[0]) + ' | ' + 'Not Attempted')
            else:
                not_attempted_user_list.append(i[0])
                # print(str(assigned_course) + ' | ' + str(completed_course) + ' | ' + 'Not Attempted')
        user_cursor.close()
        # Y-axis
        return [len(completed_user_list),len(attempted_user_list),len(not_attempted_user_list)]

    def labels(self):
        # Duplicates series on x-axis
        return self.series

    def legend(self):
        return ['User completed course', 'User attempted course', 'User not attempted course']

class ElectrogasDashboard(Dashboard):
    title = "Dashboard"
    widgets = (
        widgets.Group([UserCourseStatusChart], height=386),
        widgets.Group([LogEntryWidget, UserWidget, HistoryWidget], height=386),
        widgets.Group([AdminUserWidget, ContractorWidget, EmployeeWidget, VisitorWidget], height=350),
        widgets.Group([CourseWidget, VideoWidget, QuestionWidget], height=350),
    )

class ContractorUserWidget(widgets.ItemList):
    def user(self, obj):
        admin_url = '/overseer/induction/employee/{id}/change/'.format(id=obj.id)
        return mark_safe('<a href="{admin_url}">{username}</a>'.format(admin_url = admin_url, username = obj.username ))

    def date_joined(self, obj):
        utc_date = obj.date_joined
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%d, %b %Y')

    def time(self, obj):
        utc_date = obj.date_joined
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%I:%M %p')

    def get_queryset(self):
        if self.request.user.groups.all().filter(name='Contractor').exists():
            return Employee.objects.all().filter(groups__name__in=['Employee'], employee_contractor=self.request.user.id).order_by('-date_joined')
        elif self.request.user.groups.all().filter(name='Super Contractor').exists():
            supercontractor = SuperContractor.objects.get(pk=self.request.user.id)
            subcontractors_list = supercontractor.sc_contractor.all()
            all_contractors = []
            for i in subcontractors_list:
                sub_list = Contractor.objects.filter(contractor_company_name=i.username).values_list("id")
                sub_list = [item for t in sub_list for item in t]
                all_contractors+=[i.id]
                all_contractors+=sub_list
            subcontractors_list = all_contractors
            return Employee.objects.all().filter(groups__name__in=['Employee'], employee_contractor__in=subcontractors_list).order_by('-id')


    model = User
    list_display = ('user', 'date_joined', 'time')
    list_display_links = ['user']
    title = "Recently Registered Staff"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class ContractorHistoryWidget(widgets.ItemList):
    def user(self, obj):
        content_type = ''
        if obj.user.groups.all().filter(name='Contractor').exists():
            content_type = 'contractor'
        elif obj.user.groups.all().filter(name='Employee').exists():
            content_type = 'employee'
        elif obj.user.groups.all().filter(name='Visitor').exists():
            content_type = 'visitor'
        else:
            content_type = 'adminuser'

        admin_url = '/overseer/induction/{content_type}/{id}/change/'.format(content_type= content_type, id=obj.user.id)
        return mark_safe('<a href="{admin_url}">{username}</a>'.format(admin_url = admin_url, username = obj.user.username ))

    def user_course_complete_date(self, obj):
        utc_date = obj.user_course_complete_date
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%d, %b %Y')

    def time(self, obj):
        utc_date = obj.user_course_complete_date
        malta_date = utc_date.astimezone(pytz.timezone(settings.TIME_ZONE))
        return malta_date.strftime('%I:%M %p')

    def get_queryset(self):
        history = UserCourse.objects.all().filter(user_course_status='Completed', user_course_result='Pass', user__groups__name__in=['Employee']).order_by('-user_course_complete_date')
        result = []
        for i in history:
            employee = Employee.objects.filter(id=i.user.id).values_list('employee_contractor')
            if self.request.user.groups.all().filter(name='Contractor').exists():
                if int(employee[0][0]) == int(self.request.user.id):
                    result.append(i)
            elif self.request.user.groups.all().filter(name='Super Contractor').exists():
                supercontractor = SuperContractor.objects.get(pk=self.request.user.id)
                subcontractors_list = supercontractor.sc_contractor.all()
                all_contractors = []
                for j in subcontractors_list:
                    sub_list = Contractor.objects.filter(contractor_company_name=j.username).values_list("id")
                    sub_list = [item for t in sub_list for item in t]
                    all_contractors+=[j.id]
                    all_contractors+=sub_list
                subcontractors_list = all_contractors
                if int(employee[0][0]) in subcontractors_list:
                    result.append(i)
        return result

    model = UserCourse
    list_display = ('course', 'user', 'user_course_complete_date','time')
    title = "Recently Completed Courses by Staff"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class StaffCourseStatusChart(widgets.SinglePieChart):
    limit_to = 10
    width = widgets.LARGE

    def series(self):
        if self.request.user.groups.all().filter(name='Contractor').exists():
            user_query = '''SELECT user_ptr_id as user_id FROM induction_employee LEFT JOIN auth_user ON id = user_ptr_id WHERE employee_contractor_id = {0}'''.format(self.request.user.id)
        elif self.request.user.groups.all().filter(name='Super Contractor').exists():
            supercontractor = SuperContractor.objects.get(pk=self.request.user.id)
            subcontractors_list = supercontractor.sc_contractor.all()
            all_contractors = []
            for i in subcontractors_list:
                sub_list = Contractor.objects.filter(contractor_company_name=i.username).values_list("id")
                sub_list = [item for t in sub_list for item in t]
                all_contractors+=[i.id]
                all_contractors+=sub_list
            subcontractors_list = tuple(all_contractors)
            if len(subcontractors_list) > 1:
                user_query = '''SELECT user_ptr_id as user_id FROM induction_employee LEFT JOIN auth_user ON id = user_ptr_id WHERE employee_contractor_id in {0}'''.format(subcontractors_list)
            elif len(subcontractors_list) == 1:
                user_query = '''SELECT user_ptr_id as user_id FROM induction_employee LEFT JOIN auth_user ON id = user_ptr_id WHERE employee_contractor_id = {0}'''.format(subcontractors_list[0])
            else:
                user_query = '''SELECT user_ptr_id as user_id FROM induction_employee LEFT JOIN auth_user ON id = user_ptr_id WHERE employee_contractor_id = {0}'''.format(0)

        filter_value = datetime.now().year
        filter_dict = {'1':'current_year', '2':'previous_year', '3':'current_month', '4':'previous_month'}
        
        if self.request.GET.get('f', False) and self.request.GET.get('f', False) in filter_dict:
            if filter_dict[self.request.GET.get('f', False)] == 'current_year':
                filter_value = int(datetime.now().year)
                user_query += ' AND YEAR(date_joined) = {0}'.format(filter_value)
            elif filter_dict[self.request.GET.get('f', False)] == 'previous_year':
                filter_value = int(datetime.now().year) - 1
                user_query += ' AND YEAR(date_joined) = {0}'.format(filter_value)
            elif filter_dict[self.request.GET.get('f', False)] == 'current_month':
                filter_value = int(datetime.now().month)
                user_query += ' AND MONTH(date_joined) = {0}'.format(filter_value)
            elif filter_dict[self.request.GET.get('f', False)] == 'previous_month':
                today = datetimeobj.date.today()
                first = today.replace(day=1)
                filter_value = (first - datetimeobj.timedelta(days=1)).month
                user_query += ' AND MONTH(date_joined) = {0}'.format(filter_value)
        else:
            user_query += ' AND YEAR(date_joined) = {0}'.format(filter_value)

        user_cursor = connection.cursor()
        user_cursor.execute(user_query)
        
        completed_user_list = []
        attempted_user_list = []
        not_attempted_user_list = []

        for i in user_cursor.fetchall():
            assigned_cursor = connection.cursor()
            assigned_cursor.execute('''SELECT count(user_id) as assigned_course FROM ((SELECT induction_employee.user_ptr_id as user_id, course_id FROM induction_employee LEFT JOIN induction_contractor ON induction_contractor.user_ptr_id = induction_employee.employee_contractor_id LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id)) as grouptable WHERE course_id is not null and user_id={0} GROUP BY user_id'''.format(i[0]))
            assigned_course = assigned_cursor.fetchone()
            assigned_cursor.close()
            
            assigned_course_list_cursor = connection.cursor()
            assigned_course_list_cursor.execute('''SELECT course_id FROM ((SELECT induction_employee.user_ptr_id as user_id, course_id FROM induction_employee LEFT JOIN induction_contractor ON induction_contractor.user_ptr_id = induction_employee.employee_contractor_id LEFT JOIN induction_contractor_contractor_course ON induction_contractor_contractor_course.contractor_id = induction_contractor.user_ptr_id)) as grouptable WHERE course_id is not null and user_id={0}'''.format(i[0]))

            course_list_str = ''
            count=0
            for c in assigned_course_list_cursor.fetchall():
                count+=1
                if count== 1:
                    course_list_str += str(c[0])
                else:
                    course_list_str += ','+ str(c[0])

            completed_cursor = connection.cursor()
            completed_cursor_query = '''SELECT count(id) as completed_course FROM `induction_usercourse` WHERE user_course_status = 'Completed' and user_course_result = 'Pass' and user_id = {}'''.format(i[0])
            if len(course_list_str)>0:
                completed_cursor_query += ' AND course_id in ({})'.format(course_list_str)

            completed_cursor.execute(completed_cursor_query)
            completed_course = completed_cursor.fetchone()
            completed_cursor.close()

            attempted_cursor = connection.cursor()
            attempted_cursor_query = '''SELECT count(id) as attempted_course FROM `induction_usercourse` WHERE user_course_status = 'Attempted' and user_id = {}'''.format(i[0])
            if len(course_list_str)>0:
                attempted_cursor_query += ' AND course_id in ({})'.format(course_list_str)
                
            attempted_cursor.execute(attempted_cursor_query)
            attempted_course = attempted_cursor.fetchone()
            attempted_cursor.close()

            if assigned_course and completed_course:
                if int(assigned_course[0]) == int(completed_course[0]):
                    completed_user_list.append(i[0])
                    # print(str(assigned_course[0]) + ' | ' + str(completed_course[0]) + ' | ' + 'Completed')
                elif (int(assigned_course[0]) > int(completed_course[0])) and int(completed_course[0]) > 0:
                    attempted_user_list.append(i[0])
                    # print(str(assigned_course[0]) + ' | ' + str(completed_course[0]) + ' | ' + 'Attempted')
                else:
                    if int(attempted_course[0]) > 0:
                        attempted_user_list.append(i[0])
                    else:
                        not_attempted_user_list.append(i[0])
                    # print(str(assigned_course[0]) + ' | ' + str(completed_course[0]) + ' | ' + 'Not Attempted')
            else:
                not_attempted_user_list.append(i[0])
                # print(str(assigned_course) + ' | ' + str(completed_course) + ' | ' + 'Not Attempted')
        user_cursor.close()
        # Y-axis
        return [len(completed_user_list),len(attempted_user_list),len(not_attempted_user_list)]

    def labels(self):
        # Duplicates series on x-axis
        return self.series

    def legend(self):
        return ['Staff completed course', 'Staff attempted course', 'Staff not attempted course']

class StaffWidget(widgets.ItemList):
    def name(self,obj):
        return obj.get_full_name()

    def profile(self, obj):
        img_url = obj.employee_photo.url if obj.employee_photo else os.path.join(settings.MEDIA_URL, settings.DEFAULT_USER_IMAGE)
        return mark_safe('<img src="{url}" width="35" height="35" />'.format(url = img_url))

    def get_queryset(self):
        if self.request.user.groups.all().filter(name='Contractor').exists():
            return Employee.objects.all().filter(is_active=True, employee_contractor=self.request.user.id).order_by('-id')
        elif self.request.user.groups.all().filter(name='Super Contractor').exists():
            supercontractor = SuperContractor.objects.get(pk=self.request.user.id)
            subcontractors_list = supercontractor.sc_contractor.all()
            all_contractors = []
            for i in subcontractors_list:
                sub_list = Contractor.objects.filter(contractor_company_name=i.username).values_list("id")
                sub_list = [item for t in sub_list for item in t]
                all_contractors+=[i.id]
                all_contractors+=sub_list
            subcontractors_list = all_contractors
            return Employee.objects.all().filter(is_active=True, employee_contractor__in=subcontractors_list).order_by('-id')

    model = Employee
    list_display = ('profile', 'name', 'employee_id_card_number', 'employee_phone_number', 'employee_contractor')
    list_display_links = ['profile', 'name']
    title = "Employees"
    limit_to = 10
    sortable =  True
    width = widgets.LARGE

class ContractorDashboard(Dashboard):
    title = "Dashboard"
    widgets = (
        widgets.Group([StaffCourseStatusChart], height=386),
        widgets.Group([ContractorUserWidget, ContractorHistoryWidget], height=386),
        widgets.Group([StaffWidget], height=350),
    )